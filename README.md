# DDMC XMEGA Motherboard

This is a repository for a *Motherboard Board* from the [DDMC](https://gitlab.cba.mit.edu/jakeread/ddmc) project. 

![routed](board.png)

![schematic](schematic.png)

![parts](parts.png)

Reference Designator | Package | Type | Value | Digikey Part Number | Function | Cost
--- | --- | --- | --- | --- | --- | ---  
C3, C9, C11, C14, C16, C17, C18, C19, C20, C23, C24 | 0603C | Capacitor | 0.1uF | - | Bypass | -
C1, C2, C4, C5, C6, C7, C8, C10, C12, C13, C15, C21, C22 | 1206C | Capacitor | 10uF | - | Bypass | - 
D3, D4, D5 | 0805D | LED | - | - | Indication | - 
R1, R2, R3 R4, R6 | 0805R | Resistor | 1K | - | LED Current Limiting | -
R5, R7 | 0805R | Resistor | 120R | - | RS485 Termination | - 
S1 | SMT Right Angle | Off-Mom Switch | - | P16767CT-ND | Reset Switch | 0.32
Y1 | 3-SMD | Resonator | 16MHz | 1253-1339-1-ND | XMEGA Clock | 0.66 
D1 | SOD-123H | Power Diode | 40V 3A | FSV340FPCT-ND | Buck Assist Diode | 0.44
D2 | DO-214AC SMA | TVS Diode | 30V 400W | SMAJ30ALFDKR-ND | Transient Voltage Supression | 0.55 
U1 | QFN16 | Buck Converter | 40V -> 5V 2A | TS30042-M050QFNRCT-ND | Voltage Regulation | 1.64
U2 | SOT-25 | Regulator | AP2112 | AP2112K-3.3TRG1DIDKR-ND‎ | 5v -> 3v3 Regulation, 600mA | 0.49
U3 | QFN64 | Micro Controller | XMEGA256A3U | ATXMEGA256A3U-MH-ND | Brains | 6.94 
U4 | 16-TSSOP | Line Driver | SN65C1168 | 296-14348-6-ND | 2/2 RS-485 Driver | 1.73 
J1 | 60 Pin Mezzanine Connector | Hirose FX8-60P-SV1(91) | H10691-ND | 2.48
J4 (reverse side) | - | - | Wurth  634008137521 | 732-3164-1-ND | RJ45 Network Port | 3.32
X1 (reverse side) | USB Micro | - | Amphenol 10118192-0001LF | 609-4613-1-ND | USB Connection | 0.45