<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="borkedlabs-passives">
<packages>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="1206">
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-1.027" y1="1.245" x2="1.027" y2="1.245" width="0.1524" layer="21"/>
<wire x1="-1.002" y1="-1.245" x2="1.016" y2="-1.245" width="0.1524" layer="21"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-2.54" y="1.5875" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.302" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="R2512">
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
</package>
<package name="TO220ACS">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
2-lead molded, vertical</description>
<wire x1="5.08" y1="-1.143" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="4.953" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-4.318" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-4.699" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-4.064" x2="-5.08" y2="-1.143" width="0.1524" layer="21"/>
<circle x="-4.4958" y="-3.7084" radius="0.254" width="0" layer="21"/>
<pad name="C" x="-2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<pad name="A" x="2.54" y="-2.54" drill="1.016" shape="long" rot="R90"/>
<text x="-5.08" y="-6.0452" size="1.016" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.334" y1="-0.762" x2="5.334" y2="0" layer="21"/>
<rectangle x1="-5.334" y1="-1.27" x2="-3.429" y2="-0.762" layer="21"/>
<rectangle x1="-3.429" y1="-1.27" x2="-1.651" y2="-0.762" layer="51"/>
<rectangle x1="3.429" y1="-1.27" x2="5.334" y2="-0.762" layer="21"/>
<rectangle x1="1.651" y1="-1.27" x2="3.429" y2="-0.762" layer="51"/>
<rectangle x1="-1.651" y1="-1.27" x2="1.651" y2="-0.762" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RESISTOR">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.1524" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.1524" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.1524" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<text x="-3.81" y="-6.858" size="1.27" layer="97">&gt;PRECISION</text>
<text x="-3.81" y="-5.08" size="1.27" layer="97">&gt;PACKAGE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESISTOR" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor&lt;/b&gt;
Basic schematic elements and footprints for 0603, 1206, and PTH resistors.</description>
<gates>
<gate name="G$1" symbol="RESISTOR" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2010"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0805-RES" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-RES" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-RES" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="2512"/>
<attribute name="PRECISION" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="TO220ACS" package="TO220ACS">
<connects>
<connect gate="G$1" pin="1" pad="A"/>
<connect gate="G$1" pin="2" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lights">
<packages>
<package name="LED1206FAB">
<description>LED1206 FAB style (smaller pads to allow trace between)</description>
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="5MM">
<description>5mm round through hole part.</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="IN" x="-1.27" y="0" drill="0.8128" diameter="1.4224"/>
<pad name="OUT" x="1.27" y="0" drill="0.8128" diameter="1.4224"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED0805">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.0778" y1="0.2818" x2="0.1278" y2="0" width="0.127" layer="21"/>
<wire x1="0.1278" y1="0" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
<wire x1="-0.0778" y1="0.2818" x2="-0.0778" y2="-0.2818" width="0.127" layer="21"/>
</package>
<package name="LED1206">
<description>LED 1206 pads (standard pattern)</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<description>LED</description>
<wire x1="1.27" y1="2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="1.778" x2="-3.429" y2="0.381" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="0.635" x2="-3.302" y2="-0.762" width="0.1524" layer="94"/>
<text x="3.556" y="-2.032" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-2.032" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="0.381"/>
<vertex x="-3.048" y="1.27"/>
<vertex x="-2.54" y="0.762"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-0.762"/>
<vertex x="-2.921" y="0.127"/>
<vertex x="-2.413" y="-0.381"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="D">
<description>LED</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED1206">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB1206" package="LED1206FAB">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="5MM">
<connects>
<connect gate="G$1" pin="A" pad="IN"/>
<connect gate="G$1" pin="C" pad="OUT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="LED0805">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="CBA-SILK-LOGO">
<circle x="0" y="0" radius="0.254" width="0.127" layer="21"/>
<circle x="-0.762" y="0.762" radius="0.254" width="0.127" layer="21"/>
<wire x1="-0.254" y1="1.016" x2="0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="1.016" x2="0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="0.508" x2="-0.254" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.254" x2="-1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.508" x2="1.016" y2="0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.508" x2="1.016" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="1.016" x2="0.508" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="1.016" x2="0.508" y2="0.508" width="0.127" layer="21"/>
<wire x1="0.508" y1="0.254" x2="1.016" y2="0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.254" x2="1.016" y2="-0.254" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.254" x2="0.508" y2="0.254" width="0.127" layer="21"/>
<wire x1="0.508" y1="-0.508" x2="1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.508" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="-1.016" x2="0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.508" y1="-1.016" x2="0.508" y2="-0.508" width="0.127" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-0.508" x2="-0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.254" y1="-1.016" x2="0.254" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.254" y1="-1.016" x2="0.254" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-0.508" x2="-1.016" y2="-0.508" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-0.508" x2="-1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="-1.016" x2="-0.508" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.508" y1="-1.016" x2="-0.508" y2="-0.508" width="0.127" layer="21"/>
</package>
<package name="MK-LOGO-SILK">
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.127" layer="21"/>
<wire x1="-0.9525" y1="-1.016" x2="-0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.5715" y1="0" x2="-0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="-0.1905" y1="1.016" x2="-0.1905" y2="-1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="-1.016" x2="0.1905" y2="0" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.1905" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.9525" y2="1.016" width="0.127" layer="21"/>
<wire x1="0.1905" y1="0" x2="0.9525" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-0.5715" y1="0" x2="-0.1905" y2="1.016" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
</symbols>
<devicesets>
<deviceset name="CBA-LOGO">
<gates>
</gates>
<devices>
<device name="" package="CBA-SILK-LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MK-LOGO">
<gates>
</gates>
<devices>
<device name="" package="MK-LOGO-SILK">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3" urn="urn:adsk.eagle:symbol:26950/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC" urn="urn:adsk.eagle:symbol:26928/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V" urn="urn:adsk.eagle:symbol:26929/1" library_version="1">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" urn="urn:adsk.eagle:component:26981/1" prefix="+3V3" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" urn="urn:adsk.eagle:component:26957/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" urn="urn:adsk.eagle:component:26963/1" prefix="P+" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="power">
<packages>
<package name="SOT23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;, 5 lead</description>
<wire x1="-1.544" y1="0.713" x2="1.544" y2="0.713" width="0.1524" layer="51"/>
<wire x1="1.544" y1="0.713" x2="1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="1.544" y1="-0.712" x2="-1.544" y2="-0.712" width="0.1524" layer="51"/>
<wire x1="-1.544" y1="-0.712" x2="-1.544" y2="0.713" width="0.1524" layer="51"/>
<smd name="5" x="-0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="4" x="0.95" y="1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="1" x="-0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="2" x="0" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<smd name="3" x="0.95" y="-1.306" dx="0.5334" dy="1.1938" layer="1"/>
<text x="-1.778" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.048" y="-1.778" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.1875" y1="0.7126" x2="-0.7125" y2="1.5439" layer="51"/>
<rectangle x1="0.7125" y1="0.7126" x2="1.1875" y2="1.5439" layer="51"/>
<rectangle x1="-1.1875" y1="-1.5437" x2="-0.7125" y2="-0.7124" layer="51"/>
<rectangle x1="-0.2375" y1="-1.5437" x2="0.2375" y2="-0.7124" layer="51"/>
<rectangle x1="0.7125" y1="-1.5437" x2="1.1875" y2="-0.7124" layer="51"/>
<wire x1="-1.5" y1="-1.9" x2="-1.5" y2="-1.2" width="0.127" layer="21"/>
</package>
<package name="NRS5020T4R7MMGJ">
<smd name="1" x="0" y="1.65" dx="4.9" dy="1.5" layer="1"/>
<smd name="2" x="0" y="-1.65" dx="4.9" dy="1.5" layer="1"/>
<text x="-3" y="2.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.1" y="-4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="744777920-INDUCTOR">
<smd name="P$1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="P$2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="-4" y1="0" x2="-4" y2="3" width="0.127" layer="21"/>
<wire x1="-4" y1="3" x2="-3" y2="4" width="0.127" layer="21" curve="-90"/>
<wire x1="-3" y1="4" x2="3" y2="4" width="0.127" layer="21"/>
<wire x1="3" y1="4" x2="4" y2="3" width="0.127" layer="21" curve="-90"/>
<wire x1="4" y1="3" x2="4" y2="-3" width="0.127" layer="21"/>
<wire x1="4" y1="-3" x2="3" y2="-4" width="0.127" layer="21" curve="-90"/>
<wire x1="3" y1="-4" x2="-3" y2="-4" width="0.127" layer="21"/>
<wire x1="-3" y1="-4" x2="-4" y2="-3" width="0.127" layer="21" curve="-90"/>
<wire x1="-4" y1="-3" x2="-4" y2="0" width="0.127" layer="21"/>
<rectangle x1="-4" y1="-4" x2="4" y2="4" layer="39"/>
<text x="5.08" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="1.27" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0603-RES">
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2286" y1="-0.381" x2="0.2286" y2="0.381" layer="21"/>
</package>
<package name="SPM6530-IND">
<smd name="1" x="0" y="2.775" dx="3.4" dy="1.85" layer="1"/>
<smd name="2" x="0" y="-2.775" dx="3.4" dy="1.85" layer="1"/>
<wire x1="-3.25" y1="3.85" x2="-3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="-3.25" y1="-3.85" x2="3.25" y2="-3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="-3.85" x2="3.25" y2="3.85" width="0.127" layer="21"/>
<wire x1="3.25" y1="3.85" x2="-3.25" y2="3.85" width="0.127" layer="21"/>
<text x="3.81" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="IHLP-5050FD-01-IND">
<smd name="1" x="0" y="5.4102" dx="4.953" dy="2.9464" layer="1"/>
<smd name="2" x="0" y="-5.4102" dx="4.953" dy="2.9464" layer="1"/>
<wire x1="6.4516" y1="6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="3.81" y1="-6.604" x2="6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="6.4516" y1="6.604" x2="3.81" y2="6.604" width="0.127" layer="21"/>
<wire x1="-3.81" y1="6.604" x2="-6.4516" y2="6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="6.604" x2="-6.4516" y2="-6.604" width="0.127" layer="21"/>
<wire x1="-6.4516" y1="-6.604" x2="-3.81" y2="-6.604" width="0.127" layer="21"/>
<text x="5.08" y="7.62" size="1.016" layer="25">&gt;NAME</text>
<text x="5.08" y="-8.89" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="7443340330-IND">
<smd name="P$1" x="0" y="3.35" dx="3" dy="2.3" layer="1"/>
<smd name="P$2" x="0" y="-3.35" dx="3" dy="2.3" layer="1"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<text x="3" y="5" size="1.016" layer="25">&gt;NAME</text>
<text x="3" y="-6" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.15" y1="2.95" x2="1.15" y2="4.45" layer="51"/>
<rectangle x1="-1.15" y1="-4.45" x2="1.15" y2="-2.95" layer="51"/>
</package>
<package name="0402-RES">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-1.778" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2032" y1="-0.3556" x2="0.2032" y2="0.3556" layer="21"/>
</package>
<package name="8X8-IND">
<smd name="1" x="0" y="3.2" dx="2.2" dy="1.6" layer="1"/>
<smd name="2" x="0" y="-3.2" dx="2.2" dy="1.6" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="744029100-IND">
<smd name="1" x="0" y="1.1" dx="3.2" dy="1" layer="1"/>
<smd name="2" x="0" y="-1.1" dx="3.2" dy="1" layer="1"/>
<wire x1="-2" y1="2" x2="-2" y2="-2" width="0.127" layer="21"/>
<wire x1="-2" y1="-2" x2="2" y2="-2" width="0.127" layer="21"/>
<wire x1="2" y1="-2" x2="2" y2="2" width="0.127" layer="21"/>
<wire x1="2" y1="2" x2="-2" y2="2" width="0.127" layer="21"/>
<text x="-3" y="2.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-3" y="-3.6" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447709470-IND">
<smd name="1" x="0" y="4.95" dx="5.4" dy="2.9" layer="1"/>
<smd name="2" x="0" y="-4.95" dx="5.4" dy="2.9" layer="1"/>
<wire x1="-3" y1="6" x2="-6" y2="6" width="0.127" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.127" layer="21"/>
<wire x1="-6" y1="-6" x2="-3" y2="-6" width="0.127" layer="21"/>
<wire x1="3" y1="-6" x2="6" y2="-6" width="0.127" layer="21"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.127" layer="21"/>
<wire x1="6" y1="6" x2="3" y2="6" width="0.127" layer="21"/>
<text x="-7" y="8" size="1.27" layer="25">&gt;NAME</text>
<text x="-7" y="-9" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="7447789002-IND">
<smd name="1" x="0" y="3" dx="1.7" dy="2" layer="1"/>
<smd name="2" x="0" y="-3" dx="1.7" dy="2" layer="1"/>
<wire x1="2" y1="-4" x2="4" y2="-4" width="0.127" layer="21"/>
<wire x1="4" y1="-4" x2="4" y2="4" width="0.127" layer="21"/>
<wire x1="4" y1="4" x2="2" y2="4" width="0.127" layer="21"/>
<wire x1="-2" y1="4" x2="-4" y2="4" width="0.127" layer="21"/>
<wire x1="-4" y1="4" x2="-4" y2="-4" width="0.127" layer="21"/>
<wire x1="-4" y1="-4" x2="-2" y2="-4" width="0.127" layer="21"/>
<text x="-5" y="5" size="1.27" layer="25">&gt;NAME</text>
<text x="-5" y="-6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-3.81" x2="3.81" y2="3.81" layer="39"/>
</package>
<package name="QFN16-3X3-TI-RTE">
<description>3x3 mm</description>
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="1.025" width="0.1016" layer="51"/>
<wire x1="1.025" y1="1.5" x2="1.5" y2="1.5" width="0.1016" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="-1.025" width="0.1016" layer="51"/>
<wire x1="-1.025" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="1.025" y2="-1.5" width="0.1016" layer="51"/>
<wire x1="1.5" y1="-1.025" x2="1.5" y2="-1.5" width="0.1016" layer="51"/>
<circle x="-1.2" y="0.75" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="0.75" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="0.25" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="0.25" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="-0.25" radius="0.125" width="0" layer="31"/>
<circle x="-1.2" y="-0.75" radius="0.125" width="0" layer="31"/>
<circle x="-0.75" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.25" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.25" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.75" y="-1.2" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="-0.75" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="-0.25" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="0.25" radius="0.125" width="0" layer="31"/>
<circle x="1.2" y="0.75" radius="0.125" width="0" layer="31"/>
<circle x="0.75" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="0.25" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.25" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="-0.75" y="1.2" radius="0.125" width="0" layer="31"/>
<circle x="0" y="0" radius="0.15" width="0.01" layer="49"/>
<circle x="0.5" y="0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="0.5" y="-0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-0.5" y="-0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-0.5" y="0.5" radius="0.15" width="0.01" layer="49"/>
<circle x="-1.2" y="-0.25" radius="0.2" width="0" layer="29"/>
<circle x="-1.2" y="-0.75" radius="0.2" width="0" layer="29"/>
<circle x="-0.75" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.25" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.25" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.75" y="-1.2" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="-0.75" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="-0.25" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="0.25" radius="0.2" width="0" layer="29"/>
<circle x="1.2" y="0.75" radius="0.2" width="0" layer="29"/>
<circle x="0.75" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="0.25" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.25" y="1.2" radius="0.2" width="0" layer="29"/>
<circle x="-0.75" y="1.2" radius="0.2" width="0" layer="29"/>
<smd name="TH" x="0" y="0" dx="1.7" dy="1.7" layer="1" stop="no" cream="no"/>
<smd name="1" x="-1.475" y="0.75" dx="0.8" dy="0.275" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="2" x="-1.475" y="0.25" dx="0.8" dy="0.275" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="3" x="-1.475" y="-0.25" dx="0.8" dy="0.275" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="4" x="-1.475" y="-0.75" dx="0.8" dy="0.275" layer="1" roundness="75" stop="no" cream="no"/>
<smd name="5" x="-0.75" y="-1.475" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="6" x="-0.25" y="-1.475" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="7" x="0.25" y="-1.475" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="8" x="0.75" y="-1.475" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R90" stop="no" cream="no"/>
<smd name="9" x="1.475" y="-0.75" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="10" x="1.475" y="-0.25" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="11" x="1.475" y="0.25" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="12" x="1.475" y="0.75" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R180" stop="no" cream="no"/>
<smd name="13" x="0.75" y="1.475" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="14" x="0.25" y="1.475" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="15" x="-0.25" y="1.475" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<smd name="16" x="-0.75" y="1.475" dx="0.8" dy="0.275" layer="1" roundness="75" rot="R270" stop="no" cream="no"/>
<text x="-2" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-3.5" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.875" y1="-0.875" x2="0.875" y2="0.875" layer="29"/>
<rectangle x1="-0.8" y1="0.1" x2="-0.1" y2="0.8" layer="31" rot="R90"/>
<rectangle x1="-1.9" y1="0.55" x2="-1.2" y2="0.95" layer="29"/>
<rectangle x1="-1.85" y1="0.625" x2="-1.2" y2="0.875" layer="31"/>
<rectangle x1="-1.9" y1="0.05" x2="-1.2" y2="0.45" layer="29"/>
<rectangle x1="-1.85" y1="0.125" x2="-1.2" y2="0.375" layer="31"/>
<rectangle x1="-1.85" y1="-0.375" x2="-1.2" y2="-0.125" layer="31"/>
<rectangle x1="-1.85" y1="-0.875" x2="-1.2" y2="-0.625" layer="31"/>
<rectangle x1="-1.075" y1="-1.65" x2="-0.425" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="-0.575" y1="-1.65" x2="0.075" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="-0.075" y1="-1.65" x2="0.575" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="0.425" y1="-1.65" x2="1.075" y2="-1.4" layer="31" rot="R90"/>
<rectangle x1="1.2" y1="-0.875" x2="1.85" y2="-0.625" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="-0.375" x2="1.85" y2="-0.125" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="0.125" x2="1.85" y2="0.375" layer="31" rot="R180"/>
<rectangle x1="1.2" y1="0.625" x2="1.85" y2="0.875" layer="31" rot="R180"/>
<rectangle x1="0.425" y1="1.4" x2="1.075" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.075" y1="1.4" x2="0.575" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.575" y1="1.4" x2="0.075" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-1.075" y1="1.4" x2="-0.425" y2="1.65" layer="31" rot="R270"/>
<rectangle x1="-0.8" y1="-0.8" x2="-0.1" y2="-0.1" layer="31" rot="R180"/>
<rectangle x1="0.1" y1="-0.8" x2="0.8" y2="-0.1" layer="31" rot="R270"/>
<rectangle x1="0.1" y1="0.1" x2="0.8" y2="0.8" layer="31"/>
<rectangle x1="-1.9" y1="-0.45" x2="-1.2" y2="-0.05" layer="29"/>
<rectangle x1="-1.9" y1="-0.95" x2="-1.2" y2="-0.55" layer="29"/>
<rectangle x1="-1.1" y1="-1.75" x2="-0.4" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="-0.6" y1="-1.75" x2="0.1" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="-0.1" y1="-1.75" x2="0.6" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="0.4" y1="-1.75" x2="1.1" y2="-1.35" layer="29" rot="R90"/>
<rectangle x1="1.2" y1="-0.95" x2="1.9" y2="-0.55" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="-0.45" x2="1.9" y2="-0.05" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="0.05" x2="1.9" y2="0.45" layer="29" rot="R180"/>
<rectangle x1="1.2" y1="0.55" x2="1.9" y2="0.95" layer="29" rot="R180"/>
<rectangle x1="0.4" y1="1.35" x2="1.1" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-0.1" y1="1.35" x2="0.6" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-0.6" y1="1.35" x2="0.1" y2="1.75" layer="29" rot="R270"/>
<rectangle x1="-1.1" y1="1.35" x2="-0.4" y2="1.75" layer="29" rot="R270"/>
<wire x1="-1.5" y1="1.02" x2="-1.03" y2="1.5" width="0.127" layer="51"/>
<circle x="-2.06" y="1.24" radius="0.15" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="VREG-AP2112">
<pin name="VIN" x="-12.7" y="2.54" length="middle"/>
<pin name="EN" x="-12.7" y="-2.54" length="middle"/>
<pin name="GND" x="0" y="-10.16" length="middle" rot="R90"/>
<pin name="VOUT" x="12.7" y="2.54" length="middle" rot="R180"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<text x="-2.54" y="7.62" size="1.27" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="INDUCTOR">
<wire x1="0" y1="5.08" x2="1.27" y2="3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="2.54" x2="1.27" y2="1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94" curve="90" cap="flat"/>
<wire x1="0" y1="-2.54" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="-90" cap="flat"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-3.81" width="0.254" layer="94" curve="90" cap="flat"/>
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="7.62" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<text x="6.35" y="-5.08" size="1.27" layer="97" rot="R90">&gt;PACKAGE</text>
</symbol>
<symbol name="BUCK-TS30011-12-13">
<pin name="BST" x="15.24" y="10.16" length="middle" rot="R180"/>
<pin name="VSW" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="PG" x="15.24" y="-7.62" length="middle" rot="R180"/>
<pin name="VCC" x="-15.24" y="10.16" length="middle"/>
<pin name="EN" x="-15.24" y="2.54" length="middle"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle"/>
<pin name="PGND" x="-15.24" y="-7.62" length="middle"/>
<wire x1="-10.16" y1="12.7" x2="-10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-10.16" x2="10.16" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="12.7" x2="-10.16" y2="12.7" width="0.254" layer="94"/>
<pin name="FB" x="15.24" y="-5.08" length="middle" rot="R180"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-12.7" size="1.27" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="VREG-AP2112" prefix="U">
<gates>
<gate name="G$1" symbol="VREG-AP2112" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT23-5">
<connects>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="INDUCTOR" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="INDUCTOR" x="0" y="0"/>
</gates>
<devices>
<device name="-744777920" package="744777920-INDUCTOR">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
</technology>
</technologies>
</device>
<device name="-0603" package="0603-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
</technology>
</technologies>
</device>
<device name="-SPM6530" package="SPM6530-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IHLP-5050FD-01" package="IHLP-5050FD-01-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7443340330" package="7443340330-IND">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="7443340330"/>
</technology>
</technologies>
</device>
<device name="-0402" package="0402-RES">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
</technology>
</technologies>
</device>
<device name="-744778002" package="8X8-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-744029100" package="744029100-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447709470" package="7447709470-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-7447789002" package="7447789002-IND">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="NRS5020T4R7MMGJ">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BUCK-TS30011-12-13" prefix="U">
<gates>
<gate name="G$1" symbol="BUCK-TS30011-12-13" x="0" y="0"/>
</gates>
<devices>
<device name="QFN" package="QFN16-3X3-TI-RTE">
<connects>
<connect gate="G$1" pin="BST" pad="10"/>
<connect gate="G$1" pin="EN" pad="9"/>
<connect gate="G$1" pin="FB" pad="5"/>
<connect gate="G$1" pin="GND" pad="4 TH"/>
<connect gate="G$1" pin="PG" pad="8"/>
<connect gate="G$1" pin="PGND" pad="14 15"/>
<connect gate="G$1" pin="VCC" pad="2 3 11"/>
<connect gate="G$1" pin="VSW" pad="1 12 13 16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="microcontrollers">
<packages>
<package name="TQFP64_14X14MM-013THIN">
<description>64-Lead TQFP Plastic Thin Quad Flatpack - 14x14x1mm Body</description>
<wire x1="7" y1="7" x2="-7" y2="7" width="0.127" layer="51"/>
<wire x1="-7" y1="7" x2="-7" y2="-7" width="0.127" layer="51"/>
<wire x1="-7" y1="-7" x2="7" y2="-7" width="0.127" layer="51"/>
<wire x1="7" y1="-7" x2="7" y2="7" width="0.127" layer="51"/>
<wire x1="-7.1" y1="6.4" x2="-7.1" y2="7.1" width="0.127" layer="21"/>
<wire x1="-7.1" y1="7.1" x2="-6.4" y2="7.1" width="0.127" layer="21"/>
<wire x1="6.4" y1="7.1" x2="7.1" y2="7.1" width="0.127" layer="21"/>
<wire x1="7.1" y1="7.1" x2="7.1" y2="6.4" width="0.127" layer="21"/>
<wire x1="7.1" y1="-6.4" x2="7.1" y2="-7.1" width="0.127" layer="21"/>
<wire x1="7.1" y1="-7.1" x2="6.4" y2="-7.1" width="0.127" layer="21"/>
<wire x1="-7" y1="-6.4" x2="-6.4" y2="-7" width="0.127" layer="21"/>
<smd name="56" x="-7.6" y="0.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="57" x="-7.6" y="-0.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="58" x="-7.6" y="-1.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="55" x="-7.6" y="1.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="54" x="-7.6" y="2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="59" x="-7.6" y="-2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="60" x="-7.6" y="-2.8" dx="1.524" dy="0.3302" layer="1"/>
<smd name="53" x="-7.6" y="2.8" dx="1.524" dy="0.3302" layer="1"/>
<smd name="52" x="-7.6" y="3.6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="61" x="-7.6" y="-3.6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="62" x="-7.6" y="-4.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="51" x="-7.6" y="4.4" dx="1.524" dy="0.3302" layer="1"/>
<smd name="50" x="-7.6" y="5.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="63" x="-7.6" y="-5.2" dx="1.524" dy="0.3302" layer="1"/>
<smd name="49" x="-7.6" y="6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="64" x="-7.6" y="-6" dx="1.524" dy="0.3302" layer="1"/>
<smd name="8" x="-0.4" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="9" x="0.4" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="10" x="1.2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="7" x="-1.2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="6" x="-2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="11" x="2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="12" x="2.8" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="5" x="-2.8" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="4" x="-3.6" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="13" x="3.6" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="14" x="4.4" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="3" x="-4.4" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="2" x="-5.2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="15" x="5.2" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="1" x="-6" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="16" x="6" y="-7.6" dx="1.524" dy="0.3302" layer="1" rot="R90"/>
<smd name="24" x="7.6" y="-0.4" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="25" x="7.6" y="0.4" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="26" x="7.6" y="1.2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="23" x="7.6" y="-1.2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="22" x="7.6" y="-2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="27" x="7.6" y="2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="28" x="7.6" y="2.8" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="21" x="7.6" y="-2.8" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="20" x="7.6" y="-3.6" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="29" x="7.6" y="3.6" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="30" x="7.6" y="4.4" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="19" x="7.6" y="-4.4" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="18" x="7.6" y="-5.2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="31" x="7.6" y="5.2" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="17" x="7.6" y="-6" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="32" x="7.6" y="6" dx="1.524" dy="0.3302" layer="1" rot="R180"/>
<smd name="40" x="0.4" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="41" x="-0.4" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="42" x="-1.2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="39" x="1.2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="38" x="2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="43" x="-2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="44" x="-2.8" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="37" x="2.8" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="36" x="3.6" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="45" x="-3.6" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="46" x="-4.4" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="35" x="4.4" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="34" x="5.2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="47" x="-5.2" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="33" x="6" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<smd name="48" x="-6" y="7.6" dx="1.524" dy="0.3302" layer="1" rot="R270"/>
<text x="-8.89" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="9.525" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
<circle x="-7.62" y="-7.62" radius="0.254" width="0.127" layer="21"/>
</package>
<package name="QFN-64-9X9MM">
<description>&lt;h3&gt;64-pin QFN 9x9mm, 0.5mm pitch&lt;/h3&gt;
&lt;p&gt;Package used by ATmega128RFA1&lt;/p&gt;
&lt;p&gt;&lt;a href="http://www.atmel.com/Images/Atmel-8266-MCU_Wireless-ATmega128RFA1_Datasheet.pdf"&gt;Example Datasheet&lt;/a&gt;&lt;/p&gt;</description>
<wire x1="-4.492" y1="-4.5" x2="4.508" y2="-4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="-4.5" x2="4.508" y2="4.5" width="0.09" layer="51"/>
<wire x1="4.508" y1="4.5" x2="-4.492" y2="4.5" width="0.09" layer="51"/>
<wire x1="-4.492" y1="4.5" x2="-4.492" y2="-4.5" width="0.09" layer="51"/>
<wire x1="-4.6" y1="4.6" x2="-4.6" y2="4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="4.6" x2="-4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.1" y2="4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="4.6" x2="4.6" y2="4.1" width="0.2032" layer="21"/>
<circle x="-4.842" y="4.85" radius="0.2" width="0" layer="21"/>
<circle x="-3.442" y="3.45" radius="0.2" width="0.09" layer="51"/>
<smd name="26" x="0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="25" x="0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="24" x="-0.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="27" x="1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="28" x="1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="22" x="-1.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="21" x="-1.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="6" x="-4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="5" x="-4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="4" x="-4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="7" x="-4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="8" x="-4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="3" x="-4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="2" x="-4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="9" x="-4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="10" x="-4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="1" x="-4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="16" x="-4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="15" x="-4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="14" x="-4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="17" x="-3.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="18" x="-3.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="13" x="-4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="12" x="-4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="19" x="-2.75" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="20" x="-2.25" y="-4.5" dx="0.275" dy="0.7" layer="1" rot="R180"/>
<smd name="11" x="-4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R270"/>
<smd name="29" x="2.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="30" x="2.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="31" x="3.25" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="32" x="3.75" y="-4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="33" x="4.5" y="-3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="34" x="4.5" y="-3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="35" x="4.5" y="-2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="36" x="4.5" y="-2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="37" x="4.5" y="-1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="38" x="4.5" y="-1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="39" x="4.5" y="-0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="40" x="4.5" y="-0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="41" x="4.5" y="0.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="42" x="4.5" y="0.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="43" x="4.5" y="1.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="44" x="4.5" y="1.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="45" x="4.5" y="2.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="46" x="4.5" y="2.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="47" x="4.5" y="3.25" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="48" x="4.5" y="3.75" dx="0.275" dy="0.7" layer="1" rot="R90"/>
<smd name="49" x="3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="50" x="3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="51" x="2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="52" x="2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="53" x="1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="54" x="1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="55" x="0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="56" x="0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="57" x="-0.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="58" x="-0.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="59" x="-1.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="60" x="-1.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="61" x="-2.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="62" x="-2.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="63" x="-3.25" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<smd name="64" x="-3.75" y="4.5" dx="0.275" dy="0.7" layer="1"/>
<text x="0" y="1.27" size="0.6096" layer="25" font="vector" ratio="20" align="bottom-center">&gt;NAME</text>
<text x="0" y="-1.27" size="0.6096" layer="27" font="vector" ratio="20" align="top-center">&gt;VALUE</text>
<wire x1="4.6" y1="-4.6" x2="4.1" y2="-4.6" width="0.2032" layer="21"/>
<wire x1="4.6" y1="-4.6" x2="4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.6" y2="-4.1" width="0.2032" layer="21"/>
<wire x1="-4.6" y1="-4.6" x2="-4.1" y2="-4.6" width="0.2032" layer="21"/>
<smd name="P$1" x="0" y="0" dx="7.6" dy="7.6" layer="1" cream="no"/>
<polygon width="0.127" layer="31">
<vertex x="2.13" y="2.13"/>
<vertex x="2.13" y="3.27"/>
<vertex x="3.27" y="3.27"/>
<vertex x="3.27" y="2.13"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.27" y="2.13"/>
<vertex x="-3.27" y="3.27"/>
<vertex x="-2.13" y="3.27"/>
<vertex x="-2.13" y="2.13"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-3.27" y="-3.27"/>
<vertex x="-3.27" y="-2.13"/>
<vertex x="-2.13" y="-2.13"/>
<vertex x="-2.13" y="-3.27"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="2.13" y="-3.27"/>
<vertex x="2.13" y="-2.13"/>
<vertex x="3.27" y="-2.13"/>
<vertex x="3.27" y="-3.27"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-0.57" y="-0.57"/>
<vertex x="-0.57" y="0.57"/>
<vertex x="0.57" y="0.57"/>
<vertex x="0.57" y="-0.57"/>
</polygon>
</package>
<package name="PDI_2X3_SMD">
<description>&lt;h3&gt;Surface Mount - 2x3&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:2.54mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.81" y1="-2.5" x2="-3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.5" x2="3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.5" x2="3.81" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.5" x2="-3.81" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-0.3" y1="2.55" x2="0.3" y2="3.35" layer="51"/>
<rectangle x1="-2.84" y1="2.55" x2="-2.24" y2="3.35" layer="51"/>
<rectangle x1="2.24" y1="2.55" x2="2.84" y2="3.35" layer="51"/>
<rectangle x1="-2.84" y1="-3.35" x2="-2.24" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="-0.3" y1="-3.35" x2="0.3" y2="-2.55" layer="51" rot="R180"/>
<rectangle x1="2.24" y1="-3.35" x2="2.84" y2="-2.55" layer="51" rot="R180"/>
<smd name="1" x="-2.54" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="2" x="-2.54" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="3" x="0" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="4" x="0" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="5" x="2.54" y="-2.85" dx="1.02" dy="1.9" layer="1"/>
<smd name="6" x="2.54" y="2.85" dx="1.02" dy="1.9" layer="1"/>
<text x="-1.397" y="0.381" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.778" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-1.27" y1="-4.38" x2="1.27" y2="-4.38" width="0.2032" layer="21"/>
</package>
<package name="PDI_2X3_THRU">
<description>&lt;h3&gt;Surface Mount - 2x3&lt;/h3&gt;
&lt;p&gt;Specifications:
&lt;ul&gt;&lt;li&gt;Pin count:6&lt;/li&gt;
&lt;li&gt;Pin pitch:2.54mm&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;
&lt;p&gt;Example device(s):
&lt;ul&gt;&lt;li&gt;CONN_03x2&lt;/li&gt;
&lt;/ul&gt;&lt;/p&gt;</description>
<wire x1="-3.81" y1="-2.5" x2="-3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="-3.81" y1="2.5" x2="3.81" y2="2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="2.5" x2="3.81" y2="-2.5" width="0.127" layer="51"/>
<wire x1="3.81" y1="-2.5" x2="-3.81" y2="-2.5" width="0.127" layer="51"/>
<text x="-1.397" y="0.381" size="0.6096" layer="25" font="vector" ratio="20">&gt;NAME</text>
<text x="-1.778" y="-1.016" size="0.6096" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<wire x1="-1.27" y1="-3.11" x2="1.27" y2="-3.11" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="0" y="-1.42" drill="1.016" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="0" y="1.12" drill="1.016" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
</package>
</packages>
<symbols>
<symbol name="ATXMEGA_A3U">
<pin name="RESET/PDI_CLK" x="-25.4" y="66.04" length="middle"/>
<pin name="PDI_DATA" x="-25.4" y="60.96" length="middle"/>
<pin name="AVCC" x="-25.4" y="50.8" length="middle"/>
<pin name="VCC" x="-25.4" y="45.72" length="middle"/>
<pin name="GND" x="-25.4" y="38.1" length="middle"/>
<pin name="PR0/XTAL2" x="-25.4" y="7.62" length="middle"/>
<pin name="PR1/XTAL1" x="-25.4" y="-7.62" length="middle"/>
<pin name="PA0/AREF" x="25.4" y="66.04" length="middle" rot="R180"/>
<pin name="PA1" x="25.4" y="63.5" length="middle" rot="R180"/>
<pin name="PA2" x="25.4" y="60.96" length="middle" rot="R180"/>
<pin name="PA3" x="25.4" y="58.42" length="middle" rot="R180"/>
<pin name="PA4" x="25.4" y="55.88" length="middle" rot="R180"/>
<pin name="PA5" x="25.4" y="53.34" length="middle" rot="R180"/>
<pin name="PA6" x="25.4" y="50.8" length="middle" rot="R180"/>
<pin name="PA7" x="25.4" y="48.26" length="middle" rot="R180"/>
<pin name="PB0/AREF" x="25.4" y="43.18" length="middle" rot="R180"/>
<pin name="PB1" x="25.4" y="40.64" length="middle" rot="R180"/>
<pin name="PB2/DAC0" x="25.4" y="38.1" length="middle" rot="R180"/>
<pin name="PB3/DAC1" x="25.4" y="35.56" length="middle" rot="R180"/>
<pin name="PB4" x="25.4" y="33.02" length="middle" rot="R180"/>
<pin name="PB5" x="25.4" y="30.48" length="middle" rot="R180"/>
<pin name="PB6" x="25.4" y="27.94" length="middle" rot="R180"/>
<pin name="PB7" x="25.4" y="25.4" length="middle" rot="R180"/>
<pin name="PC0/SDA" x="25.4" y="20.32" length="middle" rot="R180"/>
<pin name="PC1/SCL/XCK0" x="25.4" y="17.78" length="middle" rot="R180"/>
<pin name="PC2/RXD0" x="25.4" y="15.24" length="middle" rot="R180"/>
<pin name="PC3/TXD0" x="25.4" y="12.7" length="middle" rot="R180"/>
<pin name="PC4/SS" x="25.4" y="10.16" length="middle" rot="R180"/>
<pin name="PC5/XCK1/MOSI" x="25.4" y="7.62" length="middle" rot="R180"/>
<pin name="PC6/RXD1/MISO" x="25.4" y="5.08" length="middle" rot="R180"/>
<pin name="PC7/TXD1/SCK" x="25.4" y="2.54" length="middle" rot="R180"/>
<pin name="PD0" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="PD1/XCK0" x="25.4" y="-5.08" length="middle" rot="R180"/>
<pin name="PD2/RXD0" x="25.4" y="-7.62" length="middle" rot="R180"/>
<pin name="PD3/TXD0" x="25.4" y="-10.16" length="middle" rot="R180"/>
<pin name="PD4/SS" x="25.4" y="-12.7" length="middle" rot="R180"/>
<pin name="PD5/XCK/MOSI" x="25.4" y="-15.24" length="middle" rot="R180"/>
<pin name="PD6/RXD1/MISO/D-" x="25.4" y="-17.78" length="middle" rot="R180"/>
<pin name="PD7/TXD1/SCK/D+" x="25.4" y="-20.32" length="middle" rot="R180"/>
<pin name="PE0/SDA" x="25.4" y="-25.4" length="middle" rot="R180"/>
<pin name="PE1/SCL/XCK0" x="25.4" y="-27.94" length="middle" rot="R180"/>
<pin name="PE2/RXD0" x="25.4" y="-30.48" length="middle" rot="R180"/>
<pin name="PE3/TXD0" x="25.4" y="-33.02" length="middle" rot="R180"/>
<pin name="PE4/SS" x="25.4" y="-35.56" length="middle" rot="R180"/>
<pin name="PE5/XCK1/MOSI" x="25.4" y="-38.1" length="middle" rot="R180"/>
<pin name="PE6/RXD1/MISO" x="25.4" y="-40.64" length="middle" rot="R180"/>
<pin name="PE7/TXD1/SCK" x="25.4" y="-43.18" length="middle" rot="R180"/>
<pin name="PF0" x="25.4" y="-48.26" length="middle" rot="R180"/>
<pin name="PF1/XCK0" x="25.4" y="-50.8" length="middle" rot="R180"/>
<pin name="PF2/RXD0" x="25.4" y="-53.34" length="middle" rot="R180"/>
<pin name="PF3/TXD0" x="25.4" y="-55.88" length="middle" rot="R180"/>
<pin name="PF4" x="25.4" y="-58.42" length="middle" rot="R180"/>
<pin name="PF5" x="25.4" y="-60.96" length="middle" rot="R180"/>
<pin name="PF6" x="25.4" y="-63.5" length="middle" rot="R180"/>
<pin name="PF7" x="25.4" y="-66.04" length="middle" rot="R180"/>
<wire x1="20.32" y1="68.58" x2="20.32" y2="-68.58" width="0.254" layer="94"/>
<wire x1="20.32" y1="-68.58" x2="-20.32" y2="-68.58" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-68.58" x2="-20.32" y2="68.58" width="0.254" layer="94"/>
<wire x1="-20.32" y1="68.58" x2="20.32" y2="68.58" width="0.254" layer="94"/>
</symbol>
<symbol name="PDI">
<description>&lt;h3&gt;6 Pin Connection&lt;/h3&gt;
3x2 pin layout</description>
<pin name="PDI_DATA" x="-15.24" y="2.54" length="middle"/>
<pin name="NC1" x="-15.24" y="0" length="middle"/>
<pin name="PDI_CLK" x="-15.24" y="-2.54" length="middle"/>
<pin name="GND" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="NC2" x="15.24" y="0" length="middle" rot="R180"/>
<pin name="VCC" x="15.24" y="2.54" length="middle" rot="R180"/>
<text x="-3.556" y="5.588" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="-3.302" y="-5.842" size="1.778" layer="96" font="vector" align="top-left">&gt;VALUE</text>
<wire x1="-11.43" y1="5.08" x2="-11.43" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="11.43" y1="-5.08" x2="-11.43" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="11.43" y1="-5.08" x2="11.43" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-11.43" y1="5.08" x2="11.43" y2="5.08" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATXMEGA_A3U" prefix="U">
<gates>
<gate name="G$1" symbol="ATXMEGA_A3U" x="0" y="0"/>
</gates>
<devices>
<device name="TQFP" package="TQFP64_14X14MM-013THIN">
<connects>
<connect gate="G$1" pin="AVCC" pad="61"/>
<connect gate="G$1" pin="GND" pad="14 24 34 44 52 60"/>
<connect gate="G$1" pin="PA0/AREF" pad="62"/>
<connect gate="G$1" pin="PA1" pad="63"/>
<connect gate="G$1" pin="PA2" pad="64"/>
<connect gate="G$1" pin="PA3" pad="1"/>
<connect gate="G$1" pin="PA4" pad="2"/>
<connect gate="G$1" pin="PA5" pad="3"/>
<connect gate="G$1" pin="PA6" pad="4"/>
<connect gate="G$1" pin="PA7" pad="5"/>
<connect gate="G$1" pin="PB0/AREF" pad="6"/>
<connect gate="G$1" pin="PB1" pad="7"/>
<connect gate="G$1" pin="PB2/DAC0" pad="8"/>
<connect gate="G$1" pin="PB3/DAC1" pad="9"/>
<connect gate="G$1" pin="PB4" pad="10"/>
<connect gate="G$1" pin="PB5" pad="11"/>
<connect gate="G$1" pin="PB6" pad="12"/>
<connect gate="G$1" pin="PB7" pad="13"/>
<connect gate="G$1" pin="PC0/SDA" pad="16"/>
<connect gate="G$1" pin="PC1/SCL/XCK0" pad="17"/>
<connect gate="G$1" pin="PC2/RXD0" pad="18"/>
<connect gate="G$1" pin="PC3/TXD0" pad="19"/>
<connect gate="G$1" pin="PC4/SS" pad="20"/>
<connect gate="G$1" pin="PC5/XCK1/MOSI" pad="21"/>
<connect gate="G$1" pin="PC6/RXD1/MISO" pad="22"/>
<connect gate="G$1" pin="PC7/TXD1/SCK" pad="23"/>
<connect gate="G$1" pin="PD0" pad="26"/>
<connect gate="G$1" pin="PD1/XCK0" pad="27"/>
<connect gate="G$1" pin="PD2/RXD0" pad="28"/>
<connect gate="G$1" pin="PD3/TXD0" pad="29"/>
<connect gate="G$1" pin="PD4/SS" pad="30"/>
<connect gate="G$1" pin="PD5/XCK/MOSI" pad="31"/>
<connect gate="G$1" pin="PD6/RXD1/MISO/D-" pad="32"/>
<connect gate="G$1" pin="PD7/TXD1/SCK/D+" pad="33"/>
<connect gate="G$1" pin="PDI_DATA" pad="56"/>
<connect gate="G$1" pin="PE0/SDA" pad="36"/>
<connect gate="G$1" pin="PE1/SCL/XCK0" pad="37"/>
<connect gate="G$1" pin="PE2/RXD0" pad="38"/>
<connect gate="G$1" pin="PE3/TXD0" pad="39"/>
<connect gate="G$1" pin="PE4/SS" pad="40"/>
<connect gate="G$1" pin="PE5/XCK1/MOSI" pad="41"/>
<connect gate="G$1" pin="PE6/RXD1/MISO" pad="42"/>
<connect gate="G$1" pin="PE7/TXD1/SCK" pad="43"/>
<connect gate="G$1" pin="PF0" pad="46"/>
<connect gate="G$1" pin="PF1/XCK0" pad="47"/>
<connect gate="G$1" pin="PF2/RXD0" pad="48"/>
<connect gate="G$1" pin="PF3/TXD0" pad="49"/>
<connect gate="G$1" pin="PF4" pad="50"/>
<connect gate="G$1" pin="PF5" pad="51"/>
<connect gate="G$1" pin="PF6" pad="54"/>
<connect gate="G$1" pin="PF7" pad="55"/>
<connect gate="G$1" pin="PR0/XTAL2" pad="58"/>
<connect gate="G$1" pin="PR1/XTAL1" pad="59"/>
<connect gate="G$1" pin="RESET/PDI_CLK" pad="57"/>
<connect gate="G$1" pin="VCC" pad="15 25 35 45 53"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="QFN" package="QFN-64-9X9MM">
<connects>
<connect gate="G$1" pin="AVCC" pad="61"/>
<connect gate="G$1" pin="GND" pad="14 24 34 44 52 60 P$1"/>
<connect gate="G$1" pin="PA0/AREF" pad="62"/>
<connect gate="G$1" pin="PA1" pad="63"/>
<connect gate="G$1" pin="PA2" pad="64"/>
<connect gate="G$1" pin="PA3" pad="1"/>
<connect gate="G$1" pin="PA4" pad="2"/>
<connect gate="G$1" pin="PA5" pad="3"/>
<connect gate="G$1" pin="PA6" pad="4"/>
<connect gate="G$1" pin="PA7" pad="5"/>
<connect gate="G$1" pin="PB0/AREF" pad="6"/>
<connect gate="G$1" pin="PB1" pad="7"/>
<connect gate="G$1" pin="PB2/DAC0" pad="8"/>
<connect gate="G$1" pin="PB3/DAC1" pad="9"/>
<connect gate="G$1" pin="PB4" pad="10"/>
<connect gate="G$1" pin="PB5" pad="11"/>
<connect gate="G$1" pin="PB6" pad="12"/>
<connect gate="G$1" pin="PB7" pad="13"/>
<connect gate="G$1" pin="PC0/SDA" pad="16"/>
<connect gate="G$1" pin="PC1/SCL/XCK0" pad="17"/>
<connect gate="G$1" pin="PC2/RXD0" pad="18"/>
<connect gate="G$1" pin="PC3/TXD0" pad="19"/>
<connect gate="G$1" pin="PC4/SS" pad="20"/>
<connect gate="G$1" pin="PC5/XCK1/MOSI" pad="21"/>
<connect gate="G$1" pin="PC6/RXD1/MISO" pad="22"/>
<connect gate="G$1" pin="PC7/TXD1/SCK" pad="23"/>
<connect gate="G$1" pin="PD0" pad="26"/>
<connect gate="G$1" pin="PD1/XCK0" pad="27"/>
<connect gate="G$1" pin="PD2/RXD0" pad="28"/>
<connect gate="G$1" pin="PD3/TXD0" pad="29"/>
<connect gate="G$1" pin="PD4/SS" pad="30"/>
<connect gate="G$1" pin="PD5/XCK/MOSI" pad="31"/>
<connect gate="G$1" pin="PD6/RXD1/MISO/D-" pad="32"/>
<connect gate="G$1" pin="PD7/TXD1/SCK/D+" pad="33"/>
<connect gate="G$1" pin="PDI_DATA" pad="56"/>
<connect gate="G$1" pin="PE0/SDA" pad="36"/>
<connect gate="G$1" pin="PE1/SCL/XCK0" pad="37"/>
<connect gate="G$1" pin="PE2/RXD0" pad="38"/>
<connect gate="G$1" pin="PE3/TXD0" pad="39"/>
<connect gate="G$1" pin="PE4/SS" pad="40"/>
<connect gate="G$1" pin="PE5/XCK1/MOSI" pad="41"/>
<connect gate="G$1" pin="PE6/RXD1/MISO" pad="42"/>
<connect gate="G$1" pin="PE7/TXD1/SCK" pad="43"/>
<connect gate="G$1" pin="PF0" pad="46"/>
<connect gate="G$1" pin="PF1/XCK0" pad="47"/>
<connect gate="G$1" pin="PF2/RXD0" pad="48"/>
<connect gate="G$1" pin="PF3/TXD0" pad="49"/>
<connect gate="G$1" pin="PF4" pad="50"/>
<connect gate="G$1" pin="PF5" pad="51"/>
<connect gate="G$1" pin="PF6" pad="54"/>
<connect gate="G$1" pin="PF7" pad="55"/>
<connect gate="G$1" pin="PR0/XTAL2" pad="58"/>
<connect gate="G$1" pin="PR1/XTAL1" pad="59"/>
<connect gate="G$1" pin="RESET/PDI_CLK" pad="57"/>
<connect gate="G$1" pin="VCC" pad="15 25 35 45 53"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PDI" prefix="J" uservalue="yes">
<description>&lt;h3&gt;Multi connection point. Often used as Generic Header-pin footprint for 0.1 inch spaced/style header connections&lt;/h3&gt;

&lt;p&gt;&lt;/p&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;You can populate with any combo of single row headers, but if you'd like an exact match, check these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/12807"&gt; Header - 2x3 (Male, 0.1")&lt;/a&gt; (PRT-12807)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/13010"&gt; Header - 2x3 (Female, 0.1")&lt;/a&gt; (PRT-13010)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10877"&gt; 2x3 Pin Shrouded Header&lt;/a&gt; (PRT-10877)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;On any of the 0.1 inch spaced packages, you can populate with these:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/116"&gt; Break Away Headers - Straight&lt;/a&gt; (PRT-00116)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/553"&gt; Break Away Male Headers - Right Angle&lt;/a&gt; (PRT-00553)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/115"&gt; Female Headers&lt;/a&gt; (PRT-00115)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/117"&gt; Break Away Headers - Machine Pin&lt;/a&gt; (PRT-00117)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/743"&gt; Break Away Female Headers - Swiss Machine Pin&lt;/a&gt; (PRT-00743)&lt;/li&gt;
&lt;/ul&gt;

&lt;p&gt;&lt;/p&gt;
&lt;b&gt;Special note: the shrouded connector mates well with our 3x2 ribbon cables:&lt;/b&gt;
&lt;ul&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10651"&gt; Ribbon Crimp Connector - 6-pin (2x3, Female)&lt;/a&gt; (PRT-10651)&lt;/li&gt;
&lt;li&gt;&lt;a href="https://www.sparkfun.com/products/10646"&gt; Ribbon Cable - 6 wire (15ft)&lt;/a&gt; (PRT-10646)&lt;/li&gt;
&lt;/ul&gt;</description>
<gates>
<gate name="G$1" symbol="PDI" x="0" y="0"/>
</gates>
<devices>
<device name="FEMALE_SMD" package="PDI_2X3_SMD">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="NC1" pad="3"/>
<connect gate="G$1" pin="NC2" pad="4"/>
<connect gate="G$1" pin="PDI_CLK" pad="5"/>
<connect gate="G$1" pin="PDI_DATA" pad="1"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PROD_ID" value="CONN-11290"/>
</technology>
</technologies>
</device>
<device name="THRU" package="PDI_2X3_THRU">
<connects>
<connect gate="G$1" pin="GND" pad="6"/>
<connect gate="G$1" pin="NC1" pad="3"/>
<connect gate="G$1" pin="NC2" pad="4"/>
<connect gate="G$1" pin="PDI_CLK" pad="5"/>
<connect gate="G$1" pin="PDI_DATA" pad="1"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="passives">
<packages>
<package name="RES_EFOBM">
<smd name="1" x="-1.35" y="0" dx="3.9" dy="0.8" layer="1" rot="R90"/>
<smd name="2" x="0" y="0" dx="3.9" dy="0.8" layer="1" rot="R270"/>
<smd name="3" x="1.35" y="0" dx="3.9" dy="0.8" layer="1" rot="R270"/>
</package>
<package name="RES_PRQC">
<smd name="1" x="-0.95" y="0" dx="1.5" dy="0.6" layer="1" rot="R90"/>
<smd name="2" x="0" y="0" dx="1.5" dy="0.4" layer="1" rot="R270"/>
<smd name="3" x="0.95" y="0" dx="1.5" dy="0.6" layer="1" rot="R270"/>
<wire x1="-1.6" y1="0.65" x2="1.6" y2="0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="0.65" x2="1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="1.6" y1="-0.65" x2="-1.6" y2="-0.65" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-0.65" x2="-1.6" y2="0.65" width="0.127" layer="51"/>
</package>
<package name="TACT-SWITCH-KMR6">
<smd name="P$1" x="-2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$2" x="2.05" y="0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$3" x="-2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<smd name="P$4" x="2.05" y="-0.8" dx="0.9" dy="1" layer="1" rot="R180"/>
<wire x1="-1.4" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="1.4" y2="0.8" width="0.127" layer="51"/>
<wire x1="-1.4" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="1.4" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="0.2" x2="-2.1" y2="-0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="-0.2" x2="2.1" y2="0.2" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.4" x2="2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="1.5" x2="1" y2="1.5" width="0.127" layer="51"/>
<wire x1="1.032" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="1.4" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.4" x2="-2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="-2.1" y1="-1.5" x2="2.1" y2="-1.5" width="0.127" layer="51"/>
<wire x1="2.1" y1="-1.5" x2="2.1" y2="-1.4" width="0.127" layer="51"/>
</package>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-0.889" x2="2.794" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.889" x2="2.794" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.889" x2="-2.921" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="0805-DIODE">
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.889" y="1.397" size="0.3048" layer="25">&gt;NAME</text>
<text x="-1.016" y="-1.143" size="0.3048" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<wire x1="-0.1778" y1="0.4318" x2="0.1778" y2="0" width="0.127" layer="21"/>
<wire x1="0.1778" y1="0" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
<wire x1="-0.1778" y1="0.4318" x2="-0.1778" y2="-0.4318" width="0.127" layer="21"/>
</package>
<package name="SOD-123HE">
<smd name="P$1" x="0.8" y="0" dx="2.4" dy="1.4" layer="1"/>
<smd name="P$2" x="-1.55" y="0" dx="0.9" dy="1.4" layer="1"/>
<wire x1="-1.4" y1="-0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="-0.9" x2="1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="-0.9" x2="1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.9" y2="0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="-1.4" y2="0.9" width="0.127" layer="51"/>
<wire x1="-1.4" y1="0.9" x2="-1.4" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0.9" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0.9" y1="0" x2="0" y2="0.9" width="0.127" layer="51"/>
<wire x1="0" y1="0.9" x2="0" y2="-0.9" width="0.127" layer="51"/>
<wire x1="0" y1="-0.9" x2="0.9" y2="0" width="0.127" layer="51"/>
<wire x1="1.4" y1="0.9" x2="0.5" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.4" y1="-0.9" x2="0.5" y2="-0.9" width="0.127" layer="21"/>
</package>
<package name="SMA-403D">
<smd name="P$1" x="-2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<smd name="P$2" x="2" y="0" dx="2" dy="2" layer="1" rot="R180"/>
<wire x1="-2.2" y1="1.2" x2="-2.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="1.6" x2="-1.2" y2="1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.2" x2="-2.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="-2.2" y1="-1.6" x2="-1.2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="-1.6" x2="2" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.2" y1="1.6" x2="2" y2="1.6" width="0.127" layer="21"/>
</package>
<package name="0603-CAP">
<wire x1="-0.356" y1="0.332" x2="0.356" y2="0.332" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.319" x2="0.356" y2="-0.319" width="0.1016" layer="51"/>
<smd name="1" x="-0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="0" dx="0.96" dy="0.8" layer="1"/>
<text x="-0.889" y="1.397" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4" x2="-0.3381" y2="0.4" layer="51"/>
<rectangle x1="0.3302" y1="-0.4" x2="0.8303" y2="0.4" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="0805">
<smd name="1" x="-0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.7" dy="1.2" layer="1"/>
<text x="-0.762" y="0.8255" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.016" y="-2.032" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="0402-CAP">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0" y1="0.0305" x2="0" y2="-0.0305" width="0.4064" layer="21"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.889" y="0.6985" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-2.413" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="1206">
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.143" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.397" y="-2.794" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="2220-C">
<smd name="P$1" x="-2.6" y="0" dx="1.2" dy="5" layer="1"/>
<smd name="P$2" x="2.6" y="0" dx="1.2" dy="5" layer="1"/>
<text x="-1.5" y="3" size="0.6096" layer="125">&gt;NAME</text>
<text x="-1.5" y="-3.5" size="0.6096" layer="127">&gt;VALUE</text>
</package>
<package name="1210">
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="1.3" x2="1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="1.6" y1="-1.3" x2="-1.6" y2="-1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="-1.6" y2="1.3" width="0.127" layer="51"/>
<wire x1="-1.6" y1="1.3" x2="1.6" y2="1.3" width="0.2032" layer="51"/>
<wire x1="-1.6" y1="-1.3" x2="1.6" y2="-1.3" width="0.2032" layer="51"/>
<smd name="1" x="-1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<smd name="2" x="1.6" y="0" dx="1.2" dy="2.5" layer="1"/>
<text x="-2.07" y="1.77" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.17" y="-3.24" size="1.016" layer="27">&gt;VALUE</text>
</package>
<package name="TACT-SWITCH-SIDE">
<smd name="P$1" x="-1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$2" x="1.8" y="0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$3" x="-1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<smd name="P$4" x="1.8" y="-0.725" dx="1.4" dy="1.05" layer="1" rot="R180"/>
<wire x1="-0.9" y1="0.8" x2="0" y2="0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0.9" y2="0.8" width="0.127" layer="51"/>
<wire x1="-0.9" y1="-0.8" x2="0" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0.9" y2="-0.8" width="0.127" layer="51"/>
<wire x1="0" y1="0.8" x2="0" y2="0.6" width="0.127" layer="51"/>
<wire x1="0" y1="0.6" x2="0.4" y2="-0.4" width="0.127" layer="51"/>
<wire x1="0" y1="-0.8" x2="0" y2="-0.5" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.45" x2="1.75" y2="-1.45" width="0.127" layer="21"/>
<wire x1="-1.75" y1="1.6" x2="-1" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="0" y2="1.6" width="0.127" layer="21"/>
<wire x1="0" y1="1.6" x2="1" y2="1.6" width="0.127" layer="21"/>
<wire x1="1" y1="1.6" x2="1.75" y2="1.6" width="0.127" layer="21"/>
<wire x1="-1" y1="1.6" x2="-1" y2="2.3" width="0.127" layer="21"/>
<wire x1="-1" y1="2.3" x2="1" y2="2.3" width="0.127" layer="21"/>
<wire x1="1" y1="2.3" x2="1" y2="1.6" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="CERAMIC_RESONATOR">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="1.27" x2="-0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-0.508" y1="-1.27" x2="0.508" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="-1.27" x2="0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0.508" y1="1.27" x2="-0.508" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="-1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.302" x2="1.778" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="1.778" y1="-2.286" x2="1.778" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.286" x2="2.54" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.302" x2="3.81" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-3.302" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.778" y1="-2.286" x2="-1.778" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.286" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.302" x2="-3.81" y2="-3.302" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-3.302" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<circle x="-3.81" y="0" radius="0.254" width="0" layer="94"/>
<circle x="3.81" y="0" radius="0.254" width="0" layer="94"/>
<circle x="0" y="-3.302" radius="0.254" width="0" layer="94"/>
<text x="-5.08" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="2" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="CAP">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.524" y="-4.064" size="1.27" layer="97">&gt;PACKAGE</text>
<text x="1.524" y="-5.842" size="1.27" layer="97">&gt;VOLTAGE</text>
<text x="1.524" y="-7.62" size="1.27" layer="97">&gt;TYPE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RESONATOR" prefix="Y">
<gates>
<gate name="G$1" symbol="CERAMIC_RESONATOR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RES_EFOBM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PQRC" package="RES_PRQC">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="2-8X4-5_SWITCH" prefix="S">
<gates>
<gate name="G$1" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TACT-SWITCH-KMR6">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDE" package="TACT-SWITCH-SIDE">
<connects>
<connect gate="G$1" pin="P" pad="P$1"/>
<connect gate="G$1" pin="P1" pad="P$2"/>
<connect gate="G$1" pin="S" pad="P$3"/>
<connect gate="G$1" pin="S1" pad="P$4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="DIODE" package="0805-DIODE">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SOD123HE" package="SOD-123HE">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMA403D" package="SMA-403D">
<connects>
<connect gate="G$1" pin="A" pad="P$2"/>
<connect gate="G$1" pin="C" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP" prefix="C" uservalue="yes">
<description>&lt;b&gt;Capacitor&lt;/b&gt;
Standard 0603 ceramic capacitor, and 0.1" leaded capacitor.</description>
<gates>
<gate name="G$1" symbol="CAP" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0805"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0603-CAP" package="0603-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0603"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="0402-CAP" package="0402-CAP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="0402"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1210" package="1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1210" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="1206" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="PACKAGE" value="1206" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
<attribute name="VOLTAGE" value="" constant="no"/>
</technology>
</technologies>
</device>
<device name="2220" package="2220-C">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="comm">
<packages>
<package name="TSSOP16">
<description>&lt;b&gt;TSOP16&lt;/b&gt;&lt;p&gt;
thin small outline package</description>
<wire x1="3.1" y1="-2.4" x2="-3" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.6" x2="3.1" y2="2.6" width="0.2032" layer="51"/>
<wire x1="3.1" y1="-2.4" x2="3.1" y2="2.6" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.6" x2="-2.8" y2="2.6" width="0.2032" layer="51"/>
<wire x1="-3" y1="2.6" x2="-3" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-3" y1="-0.5" x2="-3" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="-3" y1="-2.4" x2="-2.8" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="2.9" y1="-2.4" x2="3.1" y2="-2.4" width="0.2032" layer="51"/>
<wire x1="3.1" y1="-2.4" x2="3.1" y2="2.6" width="0.2032" layer="51"/>
<wire x1="3.1" y1="2.6" x2="2.9" y2="2.6" width="0.2032" layer="51"/>
<wire x1="-3" y1="0.5" x2="-3" y2="-0.5" width="0.2032" layer="51" curve="-180"/>
<smd name="1" x="-2.225" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="2" x="-1.575" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="3" x="-0.925" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="4" x="-0.275" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="5" x="0.375" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="6" x="1.025" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="7" x="1.675" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="8" x="2.325" y="-2.85" dx="0.4" dy="1.6" layer="1"/>
<smd name="9" x="2.325" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="10" x="1.675" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="11" x="1.025" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="12" x="0.375" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="13" x="-0.275" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="14" x="-0.925" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="15" x="-1.575" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<smd name="16" x="-2.225" y="3.05" dx="0.4" dy="1.6" layer="1"/>
<rectangle x1="-2.425" y1="-3.3501" x2="-2.0249" y2="-2.5299" layer="51"/>
<rectangle x1="-1.775" y1="-3.3501" x2="-1.3749" y2="-2.5299" layer="51"/>
<rectangle x1="-1.125" y1="-3.3501" x2="-0.725" y2="-2.5299" layer="51"/>
<rectangle x1="-0.475" y1="-3.3501" x2="-0.075" y2="-2.5299" layer="51"/>
<rectangle x1="0.175" y1="-3.3501" x2="0.575" y2="-2.5299" layer="51"/>
<rectangle x1="0.825" y1="-3.3501" x2="1.225" y2="-2.5299" layer="51"/>
<rectangle x1="1.4749" y1="-3.3501" x2="1.875" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="-3.3501" x2="2.525" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="2.7299" x2="2.525" y2="3.5501" layer="51"/>
<rectangle x1="1.4749" y1="2.7299" x2="1.875" y2="3.5501" layer="51"/>
<rectangle x1="0.825" y1="2.7299" x2="1.225" y2="3.5501" layer="51"/>
<rectangle x1="0.175" y1="2.7299" x2="0.575" y2="3.5501" layer="51"/>
<rectangle x1="-0.475" y1="2.7299" x2="-0.075" y2="3.5501" layer="51"/>
<rectangle x1="-1.125" y1="2.7299" x2="-0.725" y2="3.5501" layer="51"/>
<rectangle x1="-1.775" y1="2.7299" x2="-1.3749" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="2.7299" x2="-2.0249" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="-3.3501" x2="-2.0249" y2="-2.5299" layer="51"/>
<rectangle x1="-1.775" y1="-3.3501" x2="-1.3749" y2="-2.5299" layer="51"/>
<rectangle x1="-1.125" y1="-3.3501" x2="-0.725" y2="-2.5299" layer="51"/>
<rectangle x1="-0.475" y1="-3.3501" x2="-0.075" y2="-2.5299" layer="51"/>
<rectangle x1="0.175" y1="-3.3501" x2="0.575" y2="-2.5299" layer="51"/>
<rectangle x1="0.825" y1="-3.3501" x2="1.225" y2="-2.5299" layer="51"/>
<rectangle x1="1.4749" y1="-3.3501" x2="1.875" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="-3.3501" x2="2.525" y2="-2.5299" layer="51"/>
<rectangle x1="2.1249" y1="2.7299" x2="2.525" y2="3.5501" layer="51"/>
<rectangle x1="1.4749" y1="2.7299" x2="1.875" y2="3.5501" layer="51"/>
<rectangle x1="0.825" y1="2.7299" x2="1.225" y2="3.5501" layer="51"/>
<rectangle x1="0.175" y1="2.7299" x2="0.575" y2="3.5501" layer="51"/>
<rectangle x1="-0.475" y1="2.7299" x2="-0.075" y2="3.5501" layer="51"/>
<rectangle x1="-1.125" y1="2.7299" x2="-0.725" y2="3.5501" layer="51"/>
<rectangle x1="-1.775" y1="2.7299" x2="-1.3749" y2="3.5501" layer="51"/>
<rectangle x1="-2.425" y1="2.7299" x2="-2.0249" y2="3.5501" layer="51"/>
<text x="-3.33375" y="0" size="0.6096" layer="25" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;NAME</text>
<text x="3.96875" y="0" size="0.6096" layer="27" font="vector" ratio="20" rot="R90" align="bottom-center">&gt;VALUE</text>
<circle x="-3.2" y="-2.9" radius="0.14141875" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RS485-2BY2-SN75C1168PWR">
<pin name="1B" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="1A" x="17.78" y="25.4" length="middle" rot="R180"/>
<pin name="1R" x="-17.78" y="20.32" length="middle"/>
<pin name="1DE" x="-17.78" y="10.16" length="middle"/>
<pin name="2R" x="-17.78" y="-7.62" length="middle"/>
<pin name="2A" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="2B" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="GND" x="-17.78" y="27.94" length="middle"/>
<pin name="2D" x="-17.78" y="-22.86" length="middle"/>
<pin name="2Y" x="17.78" y="-22.86" length="middle" rot="R180"/>
<pin name="2Z" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="2DE" x="-17.78" y="-17.78" length="middle"/>
<pin name="1Z" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="1Y" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="1D" x="-17.78" y="5.08" length="middle"/>
<pin name="VCC" x="-17.78" y="35.56" length="middle"/>
<text x="-2.54" y="38.1" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="-27.94" size="1.27" layer="96">&gt;VALUE</text>
<wire x1="-12.7" y1="38.1" x2="-12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-25.4" x2="12.7" y2="-25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="-25.4" x2="12.7" y2="38.1" width="0.254" layer="94"/>
<wire x1="12.7" y1="38.1" x2="-12.7" y2="38.1" width="0.254" layer="94"/>
<wire x1="-5.08" y1="20.32" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="20.32" x2="2.54" y2="22.86" width="0.254" layer="94"/>
<wire x1="2.54" y1="22.86" x2="2.54" y2="17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="17.78" x2="0" y2="20.32" width="0.254" layer="94"/>
<wire x1="0" y1="10.16" x2="2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="2.54" y1="7.62" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-2.54" y2="5.08" width="0.254" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="0" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="2.54" y2="-5.08" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-10.16" width="0.254" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-17.78" x2="2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="2.54" y1="-20.32" x2="0" y2="-22.86" width="0.254" layer="94"/>
<wire x1="0" y1="-22.86" x2="0" y2="-20.32" width="0.254" layer="94"/>
<wire x1="0" y1="-20.32" x2="0" y2="-17.78" width="0.254" layer="94"/>
<wire x1="2.54" y1="-17.78" x2="2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-22.86" x2="-2.54" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-22.86" x2="-2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="0" y1="-20.32" x2="-2.54" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-17.78" x2="0" y2="-17.78" width="0.254" layer="94"/>
<circle x="3.302" y="18.288" radius="0.254" width="0.254" layer="94"/>
<circle x="3.302" y="5.334" radius="0.254" width="0.254" layer="94"/>
<circle x="3.302" y="-5.334" radius="0.254" width="0.254" layer="94"/>
<circle x="3.302" y="-18.034" radius="0.254" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS485-2BY2-SN75C1168PWR" prefix="U">
<gates>
<gate name="G$1" symbol="RS485-2BY2-SN75C1168PWR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TSSOP16">
<connects>
<connect gate="G$1" pin="1A" pad="2"/>
<connect gate="G$1" pin="1B" pad="1"/>
<connect gate="G$1" pin="1D" pad="15"/>
<connect gate="G$1" pin="1DE" pad="4"/>
<connect gate="G$1" pin="1R" pad="3"/>
<connect gate="G$1" pin="1Y" pad="14"/>
<connect gate="G$1" pin="1Z" pad="13"/>
<connect gate="G$1" pin="2A" pad="6"/>
<connect gate="G$1" pin="2B" pad="7"/>
<connect gate="G$1" pin="2D" pad="9"/>
<connect gate="G$1" pin="2DE" pad="12"/>
<connect gate="G$1" pin="2R" pad="5"/>
<connect gate="G$1" pin="2Y" pad="10"/>
<connect gate="G$1" pin="2Z" pad="11"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="VCC" pad="16"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connector">
<packages>
<package name="RJ45-LED-RJE7318800XXX">
<hole x="-5.74" y="0" drill="1.8"/>
<hole x="5.74" y="0" drill="1.8"/>
<pad name="SHDL1" x="-8" y="-4.35" drill="2" shape="square" rot="R180"/>
<pad name="SHDL2" x="8" y="-4.35" drill="2" shape="square"/>
<wire x1="8" y1="-6.5" x2="-8" y2="-6.5" width="0.1524" layer="51"/>
<wire x1="-8" y1="-6.5" x2="-8" y2="-9" width="0.1524" layer="51"/>
<wire x1="-8" y1="-9" x2="8" y2="-9" width="0.1524" layer="51"/>
<wire x1="8" y1="-9" x2="8" y2="6" width="0.1524" layer="51"/>
<wire x1="8" y1="6" x2="-8" y2="6" width="0.1524" layer="51"/>
<wire x1="-8" y1="6" x2="-8" y2="-6.5" width="0.1524" layer="51"/>
<pad name="P$1" x="-3.57" y="0.38" drill="0.9"/>
<pad name="P$2" x="-2.55" y="-1.45" drill="0.9"/>
<pad name="P$3" x="-1.53" y="0.38" drill="0.9"/>
<pad name="P$5" x="0.51" y="0.38" drill="0.9"/>
<pad name="P$7" x="2.55" y="0.38" drill="0.9"/>
<pad name="P$4" x="-0.51" y="-1.45" drill="0.9"/>
<pad name="P$6" x="1.53" y="-1.45" drill="0.9"/>
<pad name="P$8" x="3.57" y="-1.45" drill="0.9"/>
<pad name="YLWP" x="-7.01" y="3.6" drill="1.1"/>
<pad name="GRNN" x="7.01" y="3.6" drill="1.1"/>
<pad name="GRNP" x="4.47" y="3.6" drill="1.1"/>
<pad name="YLWN" x="-4.47" y="3.6" drill="1.1"/>
</package>
<package name="FIDUCIAL_1MM">
<smd name="1" x="0" y="0" dx="1" dy="1" layer="1" roundness="100" stop="no" cream="no"/>
<polygon width="0.127" layer="29">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<polygon width="0.127" layer="41">
<vertex x="-1" y="0" curve="90"/>
<vertex x="0" y="-1" curve="90"/>
<vertex x="1" y="0" curve="90"/>
<vertex x="0" y="1" curve="90"/>
</polygon>
<circle x="0" y="0" radius="0.4953" width="0" layer="51"/>
</package>
<package name="RJ45-LED-RJHSE-5381-UPTAB">
<hole x="-6.35" y="-2.54" drill="3.2512"/>
<hole x="6.35" y="-2.54" drill="3.2512"/>
<pad name="SHDL1" x="-8.128" y="0.889" drill="1.524" shape="square" rot="R180"/>
<pad name="SHDL2" x="8.128" y="0.889" drill="1.524" shape="square"/>
<wire x1="-8.254" y1="-6.5" x2="-8.255" y2="-8.509" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="-8.509" x2="8.255" y2="-8.509" width="0.1524" layer="51"/>
<wire x1="8.255" y1="-8.509" x2="8.255" y2="7.366" width="0.1524" layer="51"/>
<wire x1="8.255" y1="7.366" x2="-8.255" y2="7.366" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="7.366" x2="-8.254" y2="-6.5" width="0.1524" layer="51"/>
<pad name="P$1" x="3.556" y="0" drill="0.9"/>
<pad name="P$2" x="2.54" y="1.778" drill="0.9"/>
<pad name="P$3" x="1.524" y="0" drill="0.9"/>
<pad name="P$5" x="-0.508" y="0" drill="0.9"/>
<pad name="P$7" x="-2.54" y="0" drill="0.9"/>
<pad name="P$4" x="0.508" y="1.778" drill="0.9"/>
<pad name="P$6" x="-1.524" y="1.778" drill="0.9"/>
<pad name="P$8" x="-3.556" y="1.778" drill="0.9"/>
<pad name="YLWP" x="-6.858" y="6.604" drill="0.889"/>
<pad name="GRNN" x="6.858" y="6.604" drill="0.889"/>
<pad name="GRNP" x="4.572" y="6.604" drill="0.889"/>
<pad name="YLWN" x="-4.572" y="6.604" drill="0.889"/>
</package>
<package name="FIDUCIAL_RECT_1MM">
<smd name="P$1" x="0" y="0" dx="1" dy="1" layer="1"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="39"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="29"/>
<rectangle x1="-1" y1="-1" x2="1" y2="1" layer="41"/>
</package>
<package name="MINI-PCIE-ATK-MOTHER">
<smd name="15" x="-6.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="13" x="-7" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="11" x="-7.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="9" x="-8.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="7" x="-9.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="5" x="-10.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="3" x="-11" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="1" x="-11.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="17" x="-2.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="19" x="-1.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="21" x="-0.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="23" x="0.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="25" x="1" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="27" x="1.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="29" x="2.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="31" x="3.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="33" x="4.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="35" x="5" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="37" x="5.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="39" x="6.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="41" x="7.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="43" x="8.2" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="45" x="9" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="47" x="9.8" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="49" x="10.6" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="51" x="11.4" y="1.4" dx="0.55" dy="2.3" layer="1" cream="no"/>
<smd name="16" x="-5.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<wire x1="-4.6" y1="0" x2="-4.6" y2="3.25" width="0.127" layer="51"/>
<wire x1="-3.1" y1="3.25" x2="-3.1" y2="0" width="0.127" layer="51"/>
<smd name="14" x="-6.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="12" x="-7.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="10" x="-8.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="8" x="-9" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="6" x="-9.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="4" x="-10.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="2" x="-11.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="18" x="-1.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="20" x="-1" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="22" x="-0.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="24" x="0.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="26" x="1.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="28" x="2.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="30" x="3" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="32" x="3.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="34" x="4.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="36" x="5.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="38" x="6.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="40" x="7" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="42" x="7.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="44" x="8.6" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="46" x="9.4" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="48" x="10.2" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="50" x="11" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<smd name="52" x="11.8" y="1.4" dx="0.55" dy="2.3" layer="16" rot="R180" cream="no"/>
<wire x1="-3.1" y1="0" x2="12.85" y2="0" width="0.127" layer="51"/>
<wire x1="12.85" y1="0" x2="12.85" y2="3.25" width="0.127" layer="51"/>
<wire x1="15" y1="4" x2="13.6" y2="4" width="0.127" layer="51"/>
<wire x1="-13.6" y1="4" x2="-15" y2="4" width="0.127" layer="51"/>
<wire x1="-12.85" y1="3.25" x2="-13.6" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="12.85" y1="3.25" x2="13.6" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.1" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="-4.6" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-12.85" y1="3.25" x2="-12.85" y2="0" width="0.127" layer="51"/>
<wire x1="-12.85" y1="0" x2="-4.6" y2="0" width="0.127" layer="51"/>
<wire x1="-15" y1="-5.1" x2="15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="15" y1="-5.1" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="15" y1="-3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="15" y1="3.5" x2="15" y2="5.1" width="0.127" layer="51"/>
<wire x1="15" y1="5.1" x2="-15" y2="5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="5.1" x2="-15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="-15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="-15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15.8" y1="-2.5" x2="15.8" y2="-2.5" width="0.127" layer="51"/>
<rectangle x1="-15.9" y1="-5.1" x2="-13.8" y2="-2.1" layer="51"/>
<rectangle x1="13.8" y1="-5.1" x2="15.9" y2="-2.1" layer="51"/>
<pad name="P$1" x="16.5" y="8.5" drill="4.5" diameter="6.5"/>
<pad name="P$2" x="-16.5" y="8.5" drill="4.5" diameter="6.5"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="11.6"/>
<vertex x="-16.8" y="11.1" curve="-73.739933"/>
<vertex x="-17" y="10.8" curve="60.752764"/>
<vertex x="-18.8" y="8.9" curve="-80.286401"/>
<vertex x="-19.2" y="8.7" curve="6.546325"/>
<vertex x="-19.6" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="5.4"/>
<vertex x="-16.2" y="5.9" curve="-73.739933"/>
<vertex x="-16" y="6.2" curve="60.752764"/>
<vertex x="-14.2" y="8" curve="-80.286401"/>
<vertex x="-13.8" y="8.3" curve="6.546325"/>
<vertex x="-13.4" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="8.2"/>
<vertex x="-19.1" y="8.2" curve="-73.739933"/>
<vertex x="-18.8" y="8" curve="60.752764"/>
<vertex x="-17" y="6.2" curve="-80.286401"/>
<vertex x="-16.7" y="5.8" curve="6.546325"/>
<vertex x="-16.7" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="8.8"/>
<vertex x="-13.9" y="8.8" curve="-73.739933"/>
<vertex x="-14.2" y="9" curve="60.752764"/>
<vertex x="-16.1" y="10.8" curve="-80.286401"/>
<vertex x="-16.3" y="11.1" curve="6.546325"/>
<vertex x="-16.3" y="11.6" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="11.6"/>
<vertex x="16.2" y="11.1" curve="-73.739933"/>
<vertex x="16" y="10.8" curve="60.752764"/>
<vertex x="14.2" y="8.9" curve="-80.286401"/>
<vertex x="13.8" y="8.7" curve="6.546325"/>
<vertex x="13.4" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="5.4"/>
<vertex x="16.8" y="5.9" curve="-73.739933"/>
<vertex x="17" y="6.2" curve="60.752764"/>
<vertex x="18.8" y="8" curve="-80.286401"/>
<vertex x="19.2" y="8.3" curve="6.546325"/>
<vertex x="19.6" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="8.2"/>
<vertex x="13.9" y="8.2" curve="-73.739933"/>
<vertex x="14.2" y="8" curve="60.752764"/>
<vertex x="16" y="6.2" curve="-80.286401"/>
<vertex x="16.3" y="5.8" curve="6.546325"/>
<vertex x="16.3" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="8.8"/>
<vertex x="19.1" y="8.8" curve="-73.739933"/>
<vertex x="18.8" y="9" curve="60.752764"/>
<vertex x="16.9" y="10.8" curve="-80.286401"/>
<vertex x="16.7" y="11.1" curve="6.546325"/>
<vertex x="16.7" y="11.6" curve="-90"/>
</polygon>
</package>
<package name="JRTOMBSTONE">
<wire x1="-1" y1="-1" x2="-1" y2="0.7" width="0.127" layer="21"/>
<wire x1="-1" y1="0.7" x2="1" y2="0.7" width="0.127" layer="21" curve="-180"/>
<wire x1="1" y1="0.7" x2="1" y2="-1" width="0.127" layer="21"/>
<wire x1="1" y1="-1" x2="-1" y2="-1" width="0.127" layer="21"/>
</package>
<package name="DX4R005HJ5_100">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
</package>
<package name="DX4R005HJ5">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="21"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="21"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.475" y="-1.1" dx="2.75" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="D-" x="-0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="ID" x="0.65" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<smd name="GND" x="1.3" y="1.9" dx="0.4" dy="1.95" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="25" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="DX4R005HJ5_64">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="SJFAB">
<wire x1="1.016" y1="0" x2="1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="0" x2="-1.524" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="-0.127" x2="-0.254" y2="0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<wire x1="0.254" y1="0.127" x2="0.254" y2="-0.127" width="1.27" layer="51" curve="-180" cap="flat"/>
<smd name="1" x="-0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<smd name="2" x="0.7874" y="0" dx="1.1176" dy="1.6002" layer="1"/>
<text x="-1.651" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.4001" y="0" size="0.02" layer="27">&gt;VALUE</text>
</package>
<package name="MINI-PCIE-ATK-DAUGHTER">
<smd name="15" x="-6.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="13" x="-7" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="11" x="-7.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="9" x="-8.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="7" x="-9.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="5" x="-10.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="3" x="-11" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="1" x="-11.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="17" x="-2.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="19" x="-1.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="21" x="-0.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="23" x="0.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="25" x="1" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="27" x="1.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="29" x="2.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="31" x="3.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="33" x="4.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="35" x="5" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="37" x="5.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="39" x="6.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="41" x="7.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="43" x="8.2" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="45" x="9" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="47" x="9.8" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="49" x="10.6" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="51" x="11.4" y="-4.1" dx="0.45" dy="2" layer="1"/>
<smd name="16" x="-5.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<wire x1="-4.6" y1="0" x2="-4.6" y2="3.25" width="0.127" layer="51"/>
<wire x1="-3.1" y1="3.25" x2="-3.1" y2="0" width="0.127" layer="51"/>
<smd name="14" x="-6.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="12" x="-7.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="10" x="-8.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="8" x="-9" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="6" x="-9.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="4" x="-10.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="2" x="-11.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="18" x="-1.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="20" x="-1" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="22" x="-0.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="24" x="0.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="26" x="1.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="28" x="2.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="30" x="3" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="32" x="3.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="34" x="4.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="36" x="5.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="38" x="6.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="40" x="7" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="42" x="7.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="44" x="8.6" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="46" x="9.4" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="48" x="10.2" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="50" x="11" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<smd name="52" x="11.8" y="4.1" dx="0.45" dy="2" layer="1" rot="R180"/>
<wire x1="-3.1" y1="0" x2="12.85" y2="0" width="0.127" layer="51"/>
<wire x1="12.85" y1="0" x2="12.85" y2="3.25" width="0.127" layer="51"/>
<wire x1="-13.6" y1="4" x2="-19.5" y2="4" width="0.127" layer="51"/>
<wire x1="-12.85" y1="3.25" x2="-13.6" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="12.85" y1="3.25" x2="13.6" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-3.1" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="90"/>
<wire x1="-4.6" y1="3.25" x2="-3.85" y2="4" width="0.127" layer="51" curve="-90"/>
<wire x1="-12.85" y1="3.25" x2="-12.85" y2="0" width="0.127" layer="51"/>
<wire x1="-12.85" y1="0" x2="-4.6" y2="0" width="0.127" layer="51"/>
<wire x1="-15" y1="-5.1" x2="15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="15" y1="-5.1" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="15" y1="-3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="15" y1="3.5" x2="15" y2="5.1" width="0.127" layer="51"/>
<wire x1="15" y1="5.1" x2="-15" y2="5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="5.1" x2="-15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="-15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="-15" y2="-5.1" width="0.127" layer="51"/>
<wire x1="-15" y1="-3.5" x2="15" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-15" y1="3.5" x2="15" y2="3.5" width="0.127" layer="51"/>
<wire x1="-15.8" y1="-2.5" x2="15.8" y2="-2.5" width="0.127" layer="51"/>
<pad name="P$1" x="16.5" y="8.5" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$2" x="-16.5" y="8.5" drill="4.5" diameter="6.5" thermals="no"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="11.6"/>
<vertex x="-16.8" y="11.1" curve="-73.739933"/>
<vertex x="-17" y="10.8" curve="60.752764"/>
<vertex x="-18.8" y="8.9" curve="-80.286401"/>
<vertex x="-19.2" y="8.7" curve="6.546325"/>
<vertex x="-19.6" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="5.4"/>
<vertex x="-16.2" y="5.9" curve="-73.739933"/>
<vertex x="-16" y="6.2" curve="60.752764"/>
<vertex x="-14.2" y="8" curve="-80.286401"/>
<vertex x="-13.8" y="8.3" curve="6.546325"/>
<vertex x="-13.4" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="8.2"/>
<vertex x="-19.1" y="8.2" curve="-73.739933"/>
<vertex x="-18.8" y="8" curve="60.752764"/>
<vertex x="-17" y="6.2" curve="-80.286401"/>
<vertex x="-16.7" y="5.8" curve="6.546325"/>
<vertex x="-16.7" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="8.8"/>
<vertex x="-13.9" y="8.8" curve="-73.739933"/>
<vertex x="-14.2" y="9" curve="60.752764"/>
<vertex x="-16.1" y="10.8" curve="-80.286401"/>
<vertex x="-16.3" y="11.1" curve="6.546325"/>
<vertex x="-16.3" y="11.6" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="11.6"/>
<vertex x="16.2" y="11.1" curve="-73.739933"/>
<vertex x="16" y="10.8" curve="60.752764"/>
<vertex x="14.2" y="8.9" curve="-80.286401"/>
<vertex x="13.8" y="8.7" curve="6.546325"/>
<vertex x="13.4" y="8.7" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="5.4"/>
<vertex x="16.8" y="5.9" curve="-73.739933"/>
<vertex x="17" y="6.2" curve="60.752764"/>
<vertex x="18.8" y="8" curve="-80.286401"/>
<vertex x="19.2" y="8.3" curve="6.546325"/>
<vertex x="19.6" y="8.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="8.2"/>
<vertex x="13.9" y="8.2" curve="-73.739933"/>
<vertex x="14.2" y="8" curve="60.752764"/>
<vertex x="16" y="6.2" curve="-80.286401"/>
<vertex x="16.3" y="5.8" curve="6.546325"/>
<vertex x="16.3" y="5.4" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="8.8"/>
<vertex x="19.1" y="8.8" curve="-73.739933"/>
<vertex x="18.8" y="9" curve="60.752764"/>
<vertex x="16.9" y="10.8" curve="-80.286401"/>
<vertex x="16.7" y="11.1" curve="6.546325"/>
<vertex x="16.7" y="11.6" curve="-90"/>
</polygon>
<hole x="-12.5" y="0" drill="1.6"/>
<hole x="12.5" y="0" drill="1.1"/>
<smd name="P$3" x="14.65" y="-3.5" dx="2.3" dy="3.2" layer="1" rot="R180"/>
<smd name="P$4" x="-14.65" y="-3.5" dx="2.3" dy="3.2" layer="1" rot="R180"/>
<wire x1="-19.5" y1="4" x2="-20" y2="4.5" width="0.127" layer="51" curve="-90"/>
<wire x1="-20" y1="4.5" x2="-20" y2="34" width="0.127" layer="51"/>
<wire x1="-20" y1="34" x2="-19.5" y2="34.5" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="34.5" x2="19.5" y2="34.5" width="0.127" layer="51"/>
<wire x1="13.6" y1="4" x2="19.5" y2="4" width="0.127" layer="51"/>
<wire x1="19.5" y1="4" x2="20" y2="4.5" width="0.127" layer="51" curve="90"/>
<wire x1="20" y1="4.5" x2="20" y2="34" width="0.127" layer="51"/>
<wire x1="20" y1="34" x2="19.5" y2="34.5" width="0.127" layer="51" curve="90"/>
<wire x1="-19.5" y1="45" x2="19.5" y2="45" width="0.127" layer="51"/>
<wire x1="-20" y1="34" x2="-20" y2="44.5" width="0.127" layer="51"/>
<wire x1="-20" y1="44.5" x2="-19.5" y2="45" width="0.127" layer="51" curve="-90"/>
<wire x1="19.5" y1="45" x2="20" y2="44.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="44.5" x2="20" y2="34" width="0.127" layer="51"/>
</package>
<package name="ATK-BFPP-BOTTOM-FX8-60">
<smd name="P$1" x="-8.7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<pad name="P$61" x="-16.5" y="0" drill="3.5" diameter="6" thermals="no"/>
<pad name="P$62" x="16.5" y="0" drill="3.5" diameter="6" thermals="no"/>
<smd name="P$60" x="-8.7" y="2.85" dx="0.35" dy="1.7" layer="16"/>
<rectangle x1="-12.3" y1="-2.8" x2="12.3" y2="2.8" layer="52"/>
<hole x="-11.1" y="-1.8" drill="1.1"/>
<hole x="11.1" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8.1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$3" x="-7.5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$4" x="-6.9" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$5" x="-6.3" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$6" x="-5.7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$7" x="-5.1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$8" x="-4.5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$9" x="-3.9" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$10" x="-3.3" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$11" x="-2.7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$12" x="-2.1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$13" x="-1.5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$14" x="-0.9" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$15" x="-0.3" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$16" x="0.3" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$17" x="0.9" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$18" x="1.5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$19" x="2.1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$20" x="2.7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$21" x="3.3" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$22" x="3.9" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$23" x="4.5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$24" x="5.1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$25" x="5.7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$26" x="6.3" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$27" x="6.9" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$28" x="7.5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$29" x="8.1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$30" x="8.7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$31" x="8.7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$32" x="8.1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$33" x="7.5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$34" x="6.9" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$35" x="6.3" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$36" x="5.7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$37" x="5.1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$38" x="4.5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$39" x="3.9" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$40" x="3.3" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$41" x="2.7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$42" x="2.1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$43" x="1.5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$44" x="0.9" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$45" x="0.3" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$46" x="-0.3" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$47" x="-0.9" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$48" x="-1.5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$49" x="-2.1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$50" x="-2.7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$51" x="-3.3" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$52" x="-3.9" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$53" x="-4.5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$54" x="-5.1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$55" x="-5.7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$56" x="-6.3" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$57" x="-6.9" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$58" x="-7.5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$59" x="-8.1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<wire x1="-12.3" y1="-1.8" x2="-11.3" y2="-2.8" width="0.127" layer="22"/>
</package>
<package name="ATK-BFPP-THRU-FX8-60">
<smd name="P$1" x="-8.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<pad name="P$61" x="-16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$62" x="16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<smd name="P$60" x="-8.6" y="2.7" dx="0.35" dy="1.7" layer="1"/>
<rectangle x1="-12.3" y1="-2.55" x2="12.3" y2="2.55" layer="51"/>
<hole x="-11.525" y="-1.8" drill="1.1"/>
<hole x="11.525" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$3" x="-7.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$4" x="-6.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$5" x="-6.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$6" x="-5.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$7" x="-5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$8" x="-4.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$9" x="-3.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$10" x="-3.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$11" x="-2.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$12" x="-2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$13" x="-1.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$14" x="-0.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$15" x="-0.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$16" x="0.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$17" x="1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$18" x="1.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$19" x="2.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$20" x="2.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$21" x="3.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$22" x="4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$23" x="4.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$24" x="5.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$25" x="5.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$26" x="6.4" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$27" x="7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$28" x="7.6" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$29" x="8.2" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$30" x="8.8" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$31" x="8.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$32" x="8.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$33" x="7.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$34" x="7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$35" x="6.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$36" x="5.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$37" x="5.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$38" x="4.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$39" x="4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$40" x="3.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$41" x="2.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$42" x="2.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$43" x="1.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$44" x="1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$45" x="0.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$46" x="-0.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$47" x="-0.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$48" x="-1.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$49" x="-2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$50" x="-2.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$51" x="-3.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$52" x="-3.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$53" x="-4.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$54" x="-5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$55" x="-5.6" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$56" x="-6.2" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$57" x="-6.8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$58" x="-7.4" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$59" x="-8" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="3.1"/>
<vertex x="-16.8" y="2.3" curve="90"/>
<vertex x="-18.8" y="0.3"/>
<vertex x="-19.6" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="-0.3"/>
<vertex x="-18.8" y="-0.3" curve="90"/>
<vertex x="-16.8" y="-2.3"/>
<vertex x="-16.8" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="-3.1"/>
<vertex x="-16.2" y="-2.3" curve="90"/>
<vertex x="-14.2" y="-0.3"/>
<vertex x="-13.4" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="0.3"/>
<vertex x="-14.2" y="0.3" curve="90"/>
<vertex x="-16.2" y="2.3"/>
<vertex x="-16.2" y="3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="3.1"/>
<vertex x="16.2" y="2.3" curve="90"/>
<vertex x="14.2" y="0.3"/>
<vertex x="13.4" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="-0.3"/>
<vertex x="14.2" y="-0.3" curve="90"/>
<vertex x="16.2" y="-2.3"/>
<vertex x="16.2" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="-3.1"/>
<vertex x="16.8" y="-2.3" curve="90"/>
<vertex x="18.8" y="-0.3"/>
<vertex x="19.6" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="0.3"/>
<vertex x="18.8" y="0.3" curve="90"/>
<vertex x="16.8" y="2.3"/>
<vertex x="16.8" y="3.1" curve="-90"/>
</polygon>
<wire x1="-20" y1="-3.5" x2="-19.5" y2="-4" width="0.127" layer="51" curve="90"/>
<wire x1="-19.5" y1="-4" x2="19.5" y2="-4" width="0.127" layer="51"/>
<wire x1="19.5" y1="-4" x2="20" y2="-3.5" width="0.127" layer="51" curve="90"/>
<wire x1="-20" y1="-3.5" x2="-20" y2="29.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-19.5" y2="30" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="30" x2="19.5" y2="30" width="0.127" layer="51"/>
<wire x1="19.5" y1="30" x2="20" y2="29.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="29.5" x2="20" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-20" y1="29.5" x2="-20" y2="44.5" width="0.127" layer="51"/>
<wire x1="-20" y1="44.5" x2="-19.5" y2="45" width="0.127" layer="51" curve="-90"/>
<wire x1="-19.5" y1="45" x2="19.5" y2="45" width="0.127" layer="51"/>
<wire x1="19.5" y1="45" x2="20" y2="44.5" width="0.127" layer="51" curve="-90"/>
<wire x1="20" y1="44.5" x2="20" y2="29.5" width="0.127" layer="51"/>
<smd name="P$63" x="-8.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$64" x="-8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$65" x="-7.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$66" x="-6.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$67" x="-6.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$68" x="-5.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$69" x="-5" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$70" x="-4.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$71" x="-3.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$72" x="-3.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$73" x="-2.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$74" x="-2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$75" x="-1.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$76" x="-0.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$77" x="-0.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$78" x="0.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$79" x="1" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$80" x="1.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$81" x="2.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$82" x="2.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$83" x="3.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$84" x="4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$85" x="4.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$86" x="5.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$87" x="5.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$88" x="6.4" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$89" x="7" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$90" x="7.6" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$91" x="8.2" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$92" x="8.8" y="-2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$93" x="8.8" y="2.85" dx="0.35" dy="1.7" layer="16"/>
<smd name="P$94" x="8.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$95" x="7.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$96" x="7" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$97" x="6.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$98" x="5.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$99" x="5.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$100" x="4.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$101" x="4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$102" x="3.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$103" x="2.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$104" x="2.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$105" x="1.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$106" x="1" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$107" x="0.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$108" x="-0.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$109" x="-0.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$110" x="-1.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$111" x="-2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$112" x="-2.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$113" x="-3.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$114" x="-3.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$115" x="-4.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$116" x="-5" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$117" x="-5.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$118" x="-6.2" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$119" x="-6.8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$120" x="-7.4" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$121" x="-8" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<smd name="P$122" x="-8.6" y="2.85" dx="0.35" dy="1.7" layer="16" rot="R180"/>
<pad name="P$123" x="-8.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$124" x="-8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$125" x="-7.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$126" x="-6.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$127" x="-6.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$128" x="-5.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$129" x="-5" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$130" x="-4.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$131" x="-3.8" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$132" x="-3.2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$133" x="-2.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$134" x="-2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$135" x="-1.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$136" x="-0.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$137" x="-0.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$138" x="0.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$139" x="1" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$140" x="1.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$141" x="2.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$142" x="2.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$143" x="3.4" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$144" x="4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$145" x="4.6" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$146" x="5.2" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$147" x="5.8" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$148" x="6.4" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$149" x="7" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$150" x="7.6" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$151" x="8.2" y="-3.25" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$152" x="8.8" y="-2.65" drill="0.3" diameter="0.35" shape="square"/>
<pad name="P$153" x="8.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$154" x="8.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$155" x="7.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$156" x="7" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$157" x="6.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$158" x="5.8" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$159" x="5.2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$160" x="4.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$161" x="4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$162" x="3.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$163" x="2.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$164" x="2.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$165" x="1.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$166" x="1" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$167" x="0.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$168" x="-0.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$169" x="-0.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$170" x="-1.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$171" x="-2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$172" x="-2.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$173" x="-3.2" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$174" x="-3.8" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$175" x="-4.4" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$176" x="-5" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$177" x="-5.6" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$178" x="-6.2" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$179" x="-6.8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$180" x="-7.4" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$181" x="-8" y="2.65" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
<pad name="P$182" x="-8.6" y="3.25" drill="0.3" diameter="0.35" shape="square" rot="R180"/>
</package>
<package name="RJ45-LED-SMD-RJWURTH-634008137521">
<smd name="P$1" x="3.57" y="9.76" dx="4.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$2" x="2.55" y="9.76" dx="4.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$3" x="1.53" y="9.76" dx="4.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$4" x="0.51" y="9.76" dx="4.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$5" x="-0.51" y="9.76" dx="4.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$6" x="-1.53" y="9.76" dx="4.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$7" x="-2.55" y="9.76" dx="4.5" dy="0.6" layer="1" rot="R90"/>
<smd name="P$8" x="-3.57" y="9.76" dx="4.5" dy="0.6" layer="1" rot="R90"/>
<smd name="YLWP" x="-6.98" y="10.8" dx="3.05" dy="0.8" layer="1" rot="R90"/>
<smd name="YLWN" x="-5.71" y="10.8" dx="3.05" dy="0.8" layer="1" rot="R90"/>
<smd name="GRNP" x="5.71" y="10.8" dx="3.05" dy="0.8" layer="1" rot="R90"/>
<smd name="GRNN" x="6.98" y="10.8" dx="3.05" dy="0.8" layer="1" rot="R90"/>
<smd name="P$13" x="-8.65" y="0" dx="4" dy="2.5" layer="1" rot="R90"/>
<smd name="P$14" x="8.65" y="0" dx="4" dy="2.5" layer="1" rot="R90"/>
<hole x="-6.35" y="0" drill="3.25"/>
<hole x="6.35" y="0" drill="3.25"/>
<rectangle x1="-9.4" y1="-5.4" x2="9.4" y2="10.3" layer="51"/>
<smd name="P$9" x="-4.9" y="4.4" dx="1.2" dy="1.8" layer="1"/>
<smd name="P$10" x="4.9" y="4.4" dx="1.2" dy="1.8" layer="1"/>
<wire x1="-5" y1="5.6" x2="-5" y2="8.1" width="0.127" layer="21"/>
<wire x1="-5" y1="8.1" x2="-4.1" y2="8.1" width="0.127" layer="21"/>
<wire x1="5" y1="5.6" x2="5" y2="8.1" width="0.127" layer="22"/>
<wire x1="5" y1="8.1" x2="4.1" y2="8.1" width="0.127" layer="22"/>
<wire x1="-7.7" y1="10.3" x2="-9.4" y2="10.3" width="0.127" layer="21"/>
<wire x1="-9.4" y1="10.3" x2="-9.4" y2="7" width="0.127" layer="21"/>
<wire x1="-9.4" y1="-5.4" x2="-9.4" y2="-2.2" width="0.127" layer="21"/>
<wire x1="-9.4" y1="-5.4" x2="-9.1" y2="-5.4" width="0.127" layer="21"/>
<wire x1="7.7" y1="10.3" x2="9.4" y2="10.3" width="0.127" layer="22"/>
<wire x1="9.4" y1="10.3" x2="9.4" y2="7" width="0.127" layer="22"/>
<wire x1="9.4" y1="-5.4" x2="9.4" y2="-2.2" width="0.127" layer="22"/>
<wire x1="9.4" y1="-5.4" x2="9.1" y2="-5.4" width="0.127" layer="22"/>
</package>
<package name="ATK-BFPP-TOP-FX8-60">
<smd name="P$1" x="-8.7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<pad name="P$61" x="-16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<pad name="P$62" x="16.5" y="0" drill="4.5" diameter="6.5" thermals="no"/>
<smd name="P$60" x="-8.7" y="2.7" dx="0.35" dy="1.7" layer="1"/>
<rectangle x1="-12.3" y1="-2.55" x2="12.3" y2="2.55" layer="51"/>
<hole x="-11.1" y="-1.8" drill="1.1"/>
<hole x="11.1" y="-1.8" drill="0.7"/>
<smd name="P$2" x="-8.1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$3" x="-7.5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$4" x="-6.9" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$5" x="-6.3" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$6" x="-5.7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$7" x="-5.1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$8" x="-4.5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$9" x="-3.9" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$10" x="-3.3" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$11" x="-2.7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$12" x="-2.1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$13" x="-1.5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$14" x="-0.9" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$15" x="-0.3" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$16" x="0.3" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$17" x="0.9" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$18" x="1.5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$19" x="2.1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$20" x="2.7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$21" x="3.3" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$22" x="3.9" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$23" x="4.5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$24" x="5.1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$25" x="5.7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$26" x="6.3" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$27" x="6.9" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$28" x="7.5" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$29" x="8.1" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$30" x="8.7" y="-2.7" dx="0.35" dy="1.7" layer="1"/>
<smd name="P$31" x="8.7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$32" x="8.1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$33" x="7.5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$34" x="6.9" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$35" x="6.3" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$36" x="5.7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$37" x="5.1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$38" x="4.5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$39" x="3.9" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$40" x="3.3" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$41" x="2.7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$42" x="2.1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$43" x="1.5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$44" x="0.9" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$45" x="0.3" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$46" x="-0.3" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$47" x="-0.9" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$48" x="-1.5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$49" x="-2.1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$50" x="-2.7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$51" x="-3.3" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$52" x="-3.9" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$53" x="-4.5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$54" x="-5.1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$55" x="-5.7" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$56" x="-6.3" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$57" x="-6.9" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$58" x="-7.5" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<smd name="P$59" x="-8.1" y="2.7" dx="0.35" dy="1.7" layer="1" rot="R180"/>
<polygon width="0.127" layer="31">
<vertex x="-16.8" y="3.1"/>
<vertex x="-16.8" y="2.3" curve="90"/>
<vertex x="-18.8" y="0.3"/>
<vertex x="-19.6" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-19.6" y="-0.3"/>
<vertex x="-18.8" y="-0.3" curve="90"/>
<vertex x="-16.8" y="-2.3"/>
<vertex x="-16.8" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-16.2" y="-3.1"/>
<vertex x="-16.2" y="-2.3" curve="90"/>
<vertex x="-14.2" y="-0.3"/>
<vertex x="-13.4" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="-13.4" y="0.3"/>
<vertex x="-14.2" y="0.3" curve="90"/>
<vertex x="-16.2" y="2.3"/>
<vertex x="-16.2" y="3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.2" y="3.1"/>
<vertex x="16.2" y="2.3" curve="90"/>
<vertex x="14.2" y="0.3"/>
<vertex x="13.4" y="0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="13.4" y="-0.3"/>
<vertex x="14.2" y="-0.3" curve="90"/>
<vertex x="16.2" y="-2.3"/>
<vertex x="16.2" y="-3.1" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="16.8" y="-3.1"/>
<vertex x="16.8" y="-2.3" curve="90"/>
<vertex x="18.8" y="-0.3"/>
<vertex x="19.6" y="-0.3" curve="-90"/>
</polygon>
<polygon width="0.127" layer="31">
<vertex x="19.6" y="0.3"/>
<vertex x="18.8" y="0.3" curve="90"/>
<vertex x="16.8" y="2.3"/>
<vertex x="16.8" y="3.1" curve="-90"/>
</polygon>
<wire x1="-20" y1="29" x2="-19" y2="30" width="0.127" layer="51" curve="-90"/>
<wire x1="-19" y1="30" x2="19" y2="30" width="0.127" layer="51"/>
<wire x1="19" y1="30" x2="20" y2="29" width="0.127" layer="51" curve="-90"/>
<circle x="-20" y="5" radius="1.6" width="0.127" layer="51"/>
<circle x="-20" y="5" radius="2.75" width="0.127" layer="51"/>
<circle x="20" y="5" radius="1.6" width="0.127" layer="51"/>
<circle x="20" y="5" radius="2.75" width="0.127" layer="51"/>
<wire x1="-12.3" y1="-1.8" x2="-11.5" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-19.5" y1="-2.5" x2="-17.5" y2="-4.5" width="0.127" layer="51"/>
<wire x1="-17.5" y1="-4.5" x2="-16.5" y2="-5" width="0.127" layer="51" curve="36.869898"/>
<wire x1="-20" y1="-1.5" x2="-19.5" y2="-2.5" width="0.127" layer="51" curve="53.130102"/>
<wire x1="14.5" y1="-5" x2="16.5" y2="-5" width="0.127" layer="52"/>
<wire x1="19.5" y1="-2.5" x2="17.5" y2="-4.5" width="0.127" layer="52"/>
<wire x1="17.5" y1="-4.5" x2="16.5" y2="-5" width="0.127" layer="52" curve="-36.869898"/>
<wire x1="20" y1="0" x2="20" y2="-1.5" width="0.127" layer="52"/>
<wire x1="20" y1="-1.5" x2="19.5" y2="-2.5" width="0.127" layer="52" curve="-53.130102"/>
<wire x1="-20" y1="-1.5" x2="-20" y2="23" width="0.127" layer="51"/>
<wire x1="-20" y1="23" x2="-20" y2="29" width="0.127" layer="51"/>
<wire x1="20" y1="0" x2="20" y2="9.5" width="0.127" layer="51"/>
<wire x1="20" y1="9.5" x2="20" y2="18" width="0.127" layer="51"/>
<wire x1="20" y1="18" x2="20" y2="26.5" width="0.127" layer="51"/>
<wire x1="20" y1="26.5" x2="20" y2="29" width="0.127" layer="51"/>
<wire x1="14.5" y1="-5" x2="-16.5" y2="-5" width="0.127" layer="51"/>
<wire x1="-20" y1="23" x2="-17.5" y2="23" width="0.127" layer="51"/>
<wire x1="20" y1="18" x2="14.5" y2="18" width="0.127" layer="51"/>
<rectangle x1="-19.5" y1="18.5" x2="-14.5" y2="23.5" layer="51"/>
<rectangle x1="-17.5" y1="10" x2="-12.5" y2="13" layer="51"/>
<wire x1="3.5" y1="9.5" x2="3.5" y2="26.5" width="0.127" layer="51"/>
<wire x1="20" y1="26.5" x2="3.5" y2="26.5" width="0.127" layer="51"/>
<wire x1="20" y1="9.5" x2="3.5" y2="9.5" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="ATK-RS48PHYVE">
<pin name="3V3-LED" x="-10.16" y="-15.24" length="middle"/>
<pin name="LED-GRN-CATHODE" x="-10.16" y="-20.32" length="middle"/>
<pin name="LED-YLW-CATHODE" x="-10.16" y="-30.48" length="middle"/>
<pin name="CLKRX-Y" x="-10.16" y="-7.62" length="middle"/>
<pin name="CLKTX-Z" x="-10.16" y="-2.54" length="middle"/>
<pin name="RX-A" x="-10.16" y="2.54" length="middle"/>
<pin name="RX-B" x="-10.16" y="12.7" length="middle"/>
<pin name="TX-Z" x="-10.16" y="20.32" length="middle"/>
<pin name="TX-Y" x="-10.16" y="25.4" length="middle"/>
<pin name="CLKRX-B" x="-10.16" y="30.48" length="middle"/>
<pin name="CLKRX-A" x="-10.16" y="40.64" length="middle"/>
<wire x1="-5.08" y1="43.18" x2="-5.08" y2="-33.02" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-33.02" x2="20.32" y2="-33.02" width="0.254" layer="94"/>
<wire x1="20.32" y1="-33.02" x2="20.32" y2="43.18" width="0.254" layer="94"/>
<wire x1="20.32" y1="43.18" x2="-5.08" y2="43.18" width="0.254" layer="94"/>
<text x="0" y="45.72" size="1.27" layer="95">&gt;NAME</text>
<text x="0" y="-38.1" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
<symbol name="DOT">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
</symbol>
<symbol name="ATK-BFPP">
<pin name="GND" x="-10.16" y="30.48" length="middle"/>
<pin name="+3V3" x="-10.16" y="15.24" length="middle"/>
<pin name="+5V" x="-10.16" y="7.62" length="middle"/>
<pin name="A0/AREF---PB0/ADC" x="-10.16" y="0" length="middle"/>
<pin name="A1/ADC---PB1/ADC" x="-10.16" y="-2.54" length="middle"/>
<pin name="A2/ADC/DAC---PB2/ADC/DAC" x="-10.16" y="-5.08" length="middle"/>
<pin name="A3/ADC/DAC---PB3/ADC/DAC" x="-10.16" y="-7.62" length="middle"/>
<pin name="A4/ADC---PB4/ADC" x="-10.16" y="-10.16" length="middle"/>
<pin name="A5/ADC---PB5/ADC" x="-10.16" y="-12.7" length="middle"/>
<pin name="A6/ADC---PB6/ADC/ADCOUT" x="-10.16" y="-15.24" length="middle"/>
<pin name="A7/ADC---PB7/ADC/ADCOUT" x="-10.16" y="-17.78" length="middle"/>
<pin name="C0/PWMALS---PC0/PWMALS/CSN/SDA" x="-10.16" y="-22.86" length="middle"/>
<pin name="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" x="-10.16" y="-25.4" length="middle"/>
<pin name="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" x="-10.16" y="-27.94" length="middle"/>
<pin name="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" x="-10.16" y="-30.48" length="middle"/>
<pin name="C4/PWMCLS---PC4/PWMCLS/CSN" x="-10.16" y="-33.02" length="middle"/>
<pin name="C5/PWMCHS---PC5/PWMCHS/SCK" x="-10.16" y="-35.56" length="middle"/>
<pin name="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" x="-10.16" y="-38.1" length="middle"/>
<pin name="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" x="-10.16" y="-40.64" length="middle"/>
<pin name="D0/USART/CSN---PD0/PWMA/CSN" x="-10.16" y="-45.72" length="middle"/>
<pin name="D1/USART/CLK---PD1/PWMB/SCK" x="-10.16" y="-48.26" length="middle"/>
<pin name="D2/USART/RX---PD2/PWMC/MISO/UARTRX" x="-10.16" y="-50.8" length="middle"/>
<pin name="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" x="-10.16" y="-53.34" length="middle"/>
<pin name="D4/PWMELS---PD4" x="-10.16" y="-58.42" length="middle"/>
<pin name="D5/PWMEHS---PD5" x="-10.16" y="-60.96" length="middle"/>
<pin name="E0/SDA---PE0" x="-10.16" y="-66.04" length="middle"/>
<pin name="E1/SCL---PE1" x="-10.16" y="-68.58" length="middle"/>
<pin name="E2/USARTRX---PE2" x="-10.16" y="-73.66" length="middle"/>
<pin name="E3/USARTTX---PE3" x="-10.16" y="-76.2" length="middle"/>
<pin name="E4/USART/CSN---PE4/PWMA/CSN" x="-10.16" y="-81.28" length="middle"/>
<pin name="E5/USART/SCK---PE5/PWMB/SCK" x="-10.16" y="-83.82" length="middle"/>
<pin name="E6/USART/RX---PE6/UARTRX/MISO" x="-10.16" y="-86.36" length="middle"/>
<pin name="E7/USART/TX---PE7/UARTTX/MOSI" x="-10.16" y="-88.9" length="middle"/>
<pin name="VCC" x="-10.16" y="22.86" length="middle"/>
<wire x1="-5.08" y1="33.02" x2="-5.08" y2="-91.44" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-91.44" x2="45.72" y2="-91.44" width="0.254" layer="94"/>
<wire x1="45.72" y1="-91.44" x2="45.72" y2="33.02" width="0.254" layer="94"/>
<wire x1="45.72" y1="33.02" x2="-5.08" y2="33.02" width="0.254" layer="94"/>
<wire x1="27.94" y1="-2.54" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="-5.08" x2="27.94" y2="-7.62" width="0.254" layer="94"/>
<wire x1="27.94" y1="-10.16" x2="27.94" y2="-12.7" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="27.94" y2="-17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="-12.7" x2="25.4" y2="-12.7" width="0.254" layer="94"/>
<wire x1="25.4" y1="-10.16" x2="27.94" y2="-10.16" width="0.254" layer="94"/>
<wire x1="27.94" y1="-7.62" x2="25.4" y2="-7.62" width="0.254" layer="94"/>
<wire x1="25.4" y1="-5.08" x2="27.94" y2="-5.08" width="0.254" layer="94"/>
<wire x1="27.94" y1="-2.54" x2="25.4" y2="-2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="0" x2="27.94" y2="0" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.24" x2="25.4" y2="-15.24" width="0.254" layer="94"/>
<wire x1="27.94" y1="-17.78" x2="25.4" y2="-17.78" width="0.254" layer="94"/>
</symbol>
<symbol name="JRTOMBSTONE">
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94" curve="-180"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
</symbol>
<symbol name="USB-1">
<wire x1="6.35" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="-1.27" size="2.54" layer="94">USB</text>
<text x="-4.445" y="-1.905" size="1.27" layer="95" font="vector" rot="R90">&gt;Name</text>
<text x="8.255" y="-1.905" size="1.27" layer="96" font="vector" rot="R90">&gt;Value</text>
<pin name="D+" x="5.08" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="D-" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="VBUS" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="-2.54" y="5.08" visible="pad" length="short" rot="R270"/>
</symbol>
<symbol name="SJFAB">
<wire x1="0.381" y1="0.635" x2="0.381" y2="-0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="-0.381" y1="-0.635" x2="-0.381" y2="0.635" width="1.27" layer="94" curve="-180" cap="flat"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.651" y2="0" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATK-RS48PHYVE" prefix="J">
<gates>
<gate name="G$1" symbol="ATK-RS48PHYVE" x="0" y="0"/>
</gates>
<devices>
<device name="RJ45LED" package="RJ45-LED-RJE7318800XXX">
<connects>
<connect gate="G$1" pin="3V3-LED" pad="GRNP YLWP"/>
<connect gate="G$1" pin="CLKRX-A" pad="P$8"/>
<connect gate="G$1" pin="CLKRX-B" pad="P$7"/>
<connect gate="G$1" pin="CLKRX-Y" pad="P$1"/>
<connect gate="G$1" pin="CLKTX-Z" pad="P$2"/>
<connect gate="G$1" pin="LED-GRN-CATHODE" pad="GRNN"/>
<connect gate="G$1" pin="LED-YLW-CATHODE" pad="YLWN"/>
<connect gate="G$1" pin="RX-A" pad="P$3"/>
<connect gate="G$1" pin="RX-B" pad="P$4"/>
<connect gate="G$1" pin="TX-Y" pad="P$6"/>
<connect gate="G$1" pin="TX-Z" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RJ45LEDTABUP" package="RJ45-LED-RJHSE-5381-UPTAB">
<connects>
<connect gate="G$1" pin="3V3-LED" pad="GRNP YLWP"/>
<connect gate="G$1" pin="CLKRX-A" pad="P$8"/>
<connect gate="G$1" pin="CLKRX-B" pad="P$7"/>
<connect gate="G$1" pin="CLKRX-Y" pad="P$1"/>
<connect gate="G$1" pin="CLKTX-Z" pad="P$2"/>
<connect gate="G$1" pin="LED-GRN-CATHODE" pad="GRNN"/>
<connect gate="G$1" pin="LED-YLW-CATHODE" pad="YLWN"/>
<connect gate="G$1" pin="RX-A" pad="P$3"/>
<connect gate="G$1" pin="RX-B" pad="P$4"/>
<connect gate="G$1" pin="TX-Y" pad="P$6"/>
<connect gate="G$1" pin="TX-Z" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RJ45TABUPSMD" package="RJ45-LED-SMD-RJWURTH-634008137521">
<connects>
<connect gate="G$1" pin="3V3-LED" pad="GRNP YLWP"/>
<connect gate="G$1" pin="CLKRX-A" pad="P$8"/>
<connect gate="G$1" pin="CLKRX-B" pad="P$7"/>
<connect gate="G$1" pin="CLKRX-Y" pad="P$1"/>
<connect gate="G$1" pin="CLKTX-Z" pad="P$2"/>
<connect gate="G$1" pin="LED-GRN-CATHODE" pad="GRNN"/>
<connect gate="G$1" pin="LED-YLW-CATHODE" pad="YLWN"/>
<connect gate="G$1" pin="RX-A" pad="P$3"/>
<connect gate="G$1" pin="RX-B" pad="P$4"/>
<connect gate="G$1" pin="TX-Y" pad="P$6"/>
<connect gate="G$1" pin="TX-Z" pad="P$5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FIDUCIAL" prefix="J">
<description>For use by pick and place machines to calibrate the vision/machine, 1mm
&lt;p&gt;By microbuilder.eu&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="DOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FIDUCIAL_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RCT" package="FIDUCIAL_RECT_1MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ATK-BFPP" prefix="J">
<gates>
<gate name="G$1" symbol="ATK-BFPP" x="0" y="0"/>
</gates>
<devices>
<device name="MINIPCIE-MOTHER" package="MINI-PCIE-ATK-MOTHER">
<connects>
<connect gate="G$1" pin="+3V3" pad="15 16"/>
<connect gate="G$1" pin="+5V" pad="17 18"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="3"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="4"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="9"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="10"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="21"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="22"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="23"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="24"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="27"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="28"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="29"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="30"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="33"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="34"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="35"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="36"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="39"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="40"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="41"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="42"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="45"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="46"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="47"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="48"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="51"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="52"/>
<connect gate="G$1" pin="GND" pad="1 2 7 8 13 14 19 20 25 26 31 32 37 38 43 44 49 50 P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINIPCIE-DAUGHTER" package="MINI-PCIE-ATK-DAUGHTER">
<connects>
<connect gate="G$1" pin="+3V3" pad="15 16"/>
<connect gate="G$1" pin="+5V" pad="17 18"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="3"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="4"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="9"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="10"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="21"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="22"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="23"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="24"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="27"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="28"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="29"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="30"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="33"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="34"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="35"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="36"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="39"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="40"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="41"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="42"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="45"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="46"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="47"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="48"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="51"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="52"/>
<connect gate="G$1" pin="GND" pad="1 2 7 8 13 14 19 20 25 26 31 32 37 38 43 44 49 50 P$1"/>
<connect gate="G$1" pin="VCC" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-MOTHER" package="ATK-BFPP-BOTTOM-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-DAUGHTER" package="ATK-BFPP-TOP-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FX8-THRU" package="ATK-BFPP-THRU-FX8-60">
<connects>
<connect gate="G$1" pin="+3V3" pad="P$28 P$29 P$90 P$91 P$150 P$151"/>
<connect gate="G$1" pin="+5V" pad="P$26 P$27 P$88 P$89 P$148 P$149"/>
<connect gate="G$1" pin="A0/AREF---PB0/ADC" pad="P$2 P$64 P$124"/>
<connect gate="G$1" pin="A1/ADC---PB1/ADC" pad="P$3 P$65 P$125"/>
<connect gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC" pad="P$5 P$67 P$127"/>
<connect gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC" pad="P$6 P$68 P$128"/>
<connect gate="G$1" pin="A4/ADC---PB4/ADC" pad="P$8 P$70 P$130"/>
<connect gate="G$1" pin="A5/ADC---PB5/ADC" pad="P$9 P$71 P$131"/>
<connect gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT" pad="P$11 P$73 P$133"/>
<connect gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT" pad="P$12 P$74 P$134"/>
<connect gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA" pad="P$59 P$121 P$181"/>
<connect gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL" pad="P$58 P$120 P$180"/>
<connect gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX" pad="P$57 P$119 P$179"/>
<connect gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX" pad="P$56 P$118 P$178"/>
<connect gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN" pad="P$55 P$117 P$177"/>
<connect gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK" pad="P$54 P$116 P$176"/>
<connect gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX" pad="P$53 P$115 P$175"/>
<connect gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX" pad="P$52 P$114 P$174"/>
<connect gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN" pad="P$49 P$111 P$171"/>
<connect gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK" pad="P$48 P$110 P$170"/>
<connect gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX" pad="P$46 P$108 P$168"/>
<connect gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX" pad="P$45 P$107 P$167"/>
<connect gate="G$1" pin="D4/PWMELS---PD4" pad="P$15 P$77 P$137"/>
<connect gate="G$1" pin="D5/PWMEHS---PD5" pad="P$16 P$78 P$138"/>
<connect gate="G$1" pin="E0/SDA---PE0" pad="P$42 P$104 P$164"/>
<connect gate="G$1" pin="E1/SCL---PE1" pad="P$40 P$102 P$162"/>
<connect gate="G$1" pin="E2/USARTRX---PE2" pad="P$19 P$81 P$141"/>
<connect gate="G$1" pin="E3/USARTTX---PE3" pad="P$20 P$82 P$142"/>
<connect gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN" pad="P$37 P$99 P$159"/>
<connect gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK" pad="P$36 P$98 P$158"/>
<connect gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO" pad="P$34 P$96 P$156"/>
<connect gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI" pad="P$33 P$95 P$155"/>
<connect gate="G$1" pin="GND" pad="P$1 P$4 P$7 P$10 P$13 P$14 P$17 P$18 P$21 P$30 P$31 P$32 P$35 P$38 P$39 P$41 P$43 P$44 P$47 P$50 P$51 P$60 P$62 P$63 P$66 P$69 P$72 P$75 P$76 P$79 P$80 P$83 P$92 P$93 P$94 P$97 P$100 P$101 P$103 P$105 P$106 P$109 P$112 P$113 P$122 P$123 P$126 P$129 P$132 P$135 P$136 P$139 P$140 P$143 P$152 P$153 P$154 P$157 P$160 P$161 P$163 P$165 P$166 P$169 P$172 P$173 P$182"/>
<connect gate="G$1" pin="VCC" pad="P$61"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JRTOMBSTONE" prefix="MP">
<gates>
<gate name="G$1" symbol="JRTOMBSTONE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="JRTOMBSTONE">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MICRO-USB" prefix="X">
<description>SMD micro USB connector as found in the fablab inventory. 
Three footprint variants included: 
&lt;ol&gt;
&lt;li&gt; original, as described by manufacturer's datasheet
&lt;li&gt; for milling with the 1/100" bit
&lt;li&gt; for milling with the 1/64" bit
&lt;/ol&gt;
&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="USB-1" x="0" y="0"/>
</gates>
<devices>
<device name="_1/100" package="DX4R005HJ5_100">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ORIG" package="DX4R005HJ5">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1/64" package="DX4R005HJ5_64">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SOLDER_JUMPER" prefix="J">
<gates>
<gate name="G$1" symbol="SJFAB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SJFAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="P+1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C14" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C19" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C17" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C16" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="C20" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="+3V31" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="D3" library="lights" deviceset="LED" device="0805"/>
<part name="R2" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D4" library="lights" deviceset="LED" device="0805"/>
<part name="R4" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="D5" library="lights" deviceset="LED" device="0805"/>
<part name="R6" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="+3V33" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C6" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U3" library="microcontrollers" deviceset="ATXMEGA_A3U" device="QFN"/>
<part name="Y1" library="passives" deviceset="RESONATOR" device="PQRC"/>
<part name="C18" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="J101" library="microcontrollers" deviceset="PDI" device="THRU"/>
<part name="+3V32" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="S1" library="passives" deviceset="2-8X4-5_SWITCH" device="SIDE" value="2-8X4-5_SWITCHSIDE"/>
<part name="C3" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="L1" library="power" deviceset="INDUCTOR" device="" value="6.8uH"/>
<part name="C7" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C9" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U4" library="comm" deviceset="RS485-2BY2-SN75C1168PWR" device=""/>
<part name="J100" library="connector" deviceset="ATK-RS48PHYVE" device="RJ45TABUPSMD" value="ATK-RS48PHYVERJ45TABUPSMD"/>
<part name="C23" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="R7" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="R5" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="120R"/>
<part name="+3V35" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="R3" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="R1" library="borkedlabs-passives" deviceset="RESISTOR" device="0805-RES" value="150R"/>
<part name="U2" library="power" deviceset="VREG-AP2112" device=""/>
<part name="+3V34" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C11" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="J2" library="connector" deviceset="FIDUCIAL" device="RCT" value="FIDUCIALRCT"/>
<part name="J1" library="connector" deviceset="FIDUCIAL" device="RCT" value="FIDUCIALRCT"/>
<part name="+3V36" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+3V3" device=""/>
<part name="P+4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="D1" library="passives" deviceset="DIODE" device="SOD123HE"/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U1" library="power" deviceset="BUCK-TS30011-12-13" device="QFN"/>
<part name="P+6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$100" library="connector" deviceset="ATK-BFPP" device="FX8-MOTHER" value="ATK-BFPPFX8-MOTHER"/>
<part name="D2" library="passives" deviceset="DIODE" device="SMA403D"/>
<part name="P+7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="VCC" device=""/>
<part name="MP100" library="connector" deviceset="JRTOMBSTONE" device=""/>
<part name="X100" library="connector" deviceset="MICRO-USB" device="_ORIG"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="P+5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="P+2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="+5V" device=""/>
<part name="J102" library="connector" deviceset="SOLDER_JUMPER" device=""/>
<part name="C22" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C21" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C24" library="passives" deviceset="CAP" device="0603-CAP" value="0.1uF"/>
<part name="GND14" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C5" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C10" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C8" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C4" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C2" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C1" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C15" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C13" library="passives" deviceset="CAP" device="1206" value="10uF"/>
<part name="C12" library="passives" deviceset="CAP" device="1206" value="10uF"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="P+1" gate="VCC" x="-35.56" y="302.26" smashed="yes" rot="R90">
<attribute name="VALUE" x="-33.02" y="299.72" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND1" gate="1" x="-35.56" y="292.1" smashed="yes" rot="R270">
<attribute name="VALUE" x="-38.1" y="294.64" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C14" gate="G$1" x="25.4" y="147.32" smashed="yes">
<attribute name="NAME" x="26.924" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="26.924" y="145.161" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="26.924" y="143.256" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="26.924" y="141.478" size="1.27" layer="97"/>
<attribute name="TYPE" x="26.924" y="139.7" size="1.27" layer="97"/>
</instance>
<instance part="C19" gate="G$1" x="15.24" y="147.32" smashed="yes">
<attribute name="NAME" x="16.764" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="16.764" y="145.161" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="16.764" y="143.256" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="16.764" y="141.478" size="1.27" layer="97"/>
<attribute name="TYPE" x="16.764" y="139.7" size="1.27" layer="97"/>
</instance>
<instance part="C17" gate="G$1" x="5.08" y="147.32" smashed="yes">
<attribute name="NAME" x="6.604" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="6.604" y="145.161" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="6.604" y="143.256" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="6.604" y="141.478" size="1.27" layer="97"/>
<attribute name="TYPE" x="6.604" y="139.7" size="1.27" layer="97"/>
</instance>
<instance part="C16" gate="G$1" x="-5.08" y="147.32" smashed="yes">
<attribute name="NAME" x="-3.556" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="-3.556" y="145.161" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="-3.556" y="143.256" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="-3.556" y="141.478" size="1.27" layer="97"/>
<attribute name="TYPE" x="-3.556" y="139.7" size="1.27" layer="97"/>
</instance>
<instance part="C20" gate="G$1" x="-15.24" y="147.32" smashed="yes">
<attribute name="NAME" x="-13.716" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="-13.716" y="145.161" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="-13.716" y="143.256" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="-13.716" y="141.478" size="1.27" layer="97"/>
<attribute name="TYPE" x="-13.716" y="139.7" size="1.27" layer="97"/>
</instance>
<instance part="GND3" gate="1" x="-27.94" y="172.72" smashed="yes" rot="R270">
<attribute name="VALUE" x="-30.48" y="175.26" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND2" gate="1" x="-45.72" y="144.78" smashed="yes" rot="R270">
<attribute name="VALUE" x="-48.26" y="147.32" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="+3V31" gate="G$1" x="-45.72" y="152.4" smashed="yes" rot="R90">
<attribute name="VALUE" x="-40.64" y="149.86" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D3" gate="G$1" x="60.96" y="281.94" smashed="yes" rot="R270">
<attribute name="NAME" x="58.928" y="278.384" size="1.778" layer="95"/>
<attribute name="VALUE" x="58.928" y="276.225" size="1.778" layer="96"/>
</instance>
<instance part="R2" gate="G$1" x="48.26" y="281.94" smashed="yes">
<attribute name="NAME" x="44.45" y="283.4386" size="1.778" layer="95"/>
<attribute name="VALUE" x="44.45" y="278.638" size="1.778" layer="96"/>
<attribute name="PRECISION" x="44.45" y="275.082" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="44.45" y="276.86" size="1.27" layer="97"/>
</instance>
<instance part="D4" gate="G$1" x="60.96" y="292.1" smashed="yes" rot="R270">
<attribute name="NAME" x="58.928" y="288.544" size="1.778" layer="95"/>
<attribute name="VALUE" x="58.928" y="286.385" size="1.778" layer="96"/>
</instance>
<instance part="R4" gate="G$1" x="48.26" y="292.1" smashed="yes">
<attribute name="NAME" x="44.45" y="293.5986" size="1.778" layer="95"/>
<attribute name="VALUE" x="44.45" y="288.798" size="1.778" layer="96"/>
<attribute name="PRECISION" x="44.45" y="285.242" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="44.45" y="287.02" size="1.27" layer="97"/>
</instance>
<instance part="D5" gate="G$1" x="60.96" y="302.26" smashed="yes" rot="R270">
<attribute name="NAME" x="58.928" y="298.704" size="1.778" layer="95"/>
<attribute name="VALUE" x="58.928" y="296.545" size="1.778" layer="96"/>
</instance>
<instance part="R6" gate="G$1" x="48.26" y="302.26" smashed="yes">
<attribute name="NAME" x="44.45" y="303.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="44.45" y="298.958" size="1.778" layer="96"/>
<attribute name="PRECISION" x="44.45" y="295.402" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="44.45" y="297.18" size="1.27" layer="97"/>
</instance>
<instance part="+3V33" gate="G$1" x="83.82" y="287.02" smashed="yes" rot="R270">
<attribute name="VALUE" x="78.74" y="289.56" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="30.48" y="302.26" smashed="yes" rot="R270">
<attribute name="VALUE" x="27.94" y="304.8" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C6" gate="G$1" x="-7.62" y="297.18" smashed="yes">
<attribute name="NAME" x="-6.096" y="300.101" size="1.778" layer="95"/>
<attribute name="VALUE" x="-6.096" y="295.021" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="-6.096" y="293.116" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="-6.096" y="291.338" size="1.27" layer="97"/>
<attribute name="TYPE" x="-6.096" y="289.56" size="1.27" layer="97"/>
</instance>
<instance part="GND4" gate="1" x="15.24" y="106.68" smashed="yes" rot="R270">
<attribute name="VALUE" x="12.7" y="109.22" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U3" gate="G$1" x="63.5" y="106.68" smashed="yes"/>
<instance part="Y1" gate="G$1" x="30.48" y="106.68" smashed="yes" rot="R270">
<attribute name="NAME" x="34.29" y="111.76" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="22.86" y="104.14" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C18" gate="G$1" x="-25.4" y="147.32" smashed="yes">
<attribute name="NAME" x="-23.876" y="150.241" size="1.778" layer="95"/>
<attribute name="VALUE" x="-23.876" y="145.161" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="-23.876" y="143.256" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="-23.876" y="141.478" size="1.27" layer="97"/>
<attribute name="TYPE" x="-23.876" y="139.7" size="1.27" layer="97"/>
</instance>
<instance part="J101" gate="G$1" x="5.08" y="170.18" smashed="yes" rot="R180">
<attribute name="NAME" x="8.636" y="164.592" size="1.778" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="8.382" y="176.022" size="1.778" layer="96" font="vector" rot="R180" align="top-left"/>
</instance>
<instance part="+3V32" gate="G$1" x="-27.94" y="167.64" smashed="yes" rot="R90">
<attribute name="VALUE" x="-22.86" y="165.1" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="S1" gate="G$1" x="5.08" y="182.88" smashed="yes" rot="R270">
<attribute name="NAME" x="2.54" y="189.23" size="1.778" layer="95"/>
<attribute name="VALUE" x="8.255" y="186.69" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="G$1" x="-35.56" y="256.54" smashed="yes">
<attribute name="NAME" x="-34.036" y="259.461" size="1.778" layer="95"/>
<attribute name="VALUE" x="-34.036" y="254.381" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="-34.036" y="252.476" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="-34.036" y="250.698" size="1.27" layer="97"/>
<attribute name="TYPE" x="-34.036" y="248.92" size="1.27" layer="97"/>
</instance>
<instance part="L1" gate="G$1" x="-17.78" y="254" smashed="yes" rot="R270">
<attribute name="NAME" x="-22.86" y="255.27" size="1.778" layer="95"/>
<attribute name="VALUE" x="-22.86" y="250.19" size="1.778" layer="96"/>
</instance>
<instance part="C7" gate="G$1" x="-17.78" y="297.18" smashed="yes">
<attribute name="NAME" x="-16.256" y="300.101" size="1.778" layer="95"/>
<attribute name="VALUE" x="-16.256" y="295.021" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="-16.256" y="293.116" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="-16.256" y="291.338" size="1.27" layer="97"/>
<attribute name="TYPE" x="-16.256" y="289.56" size="1.27" layer="97"/>
</instance>
<instance part="C9" gate="G$1" x="-2.54" y="248.92" smashed="yes">
<attribute name="NAME" x="-1.016" y="251.841" size="1.778" layer="95"/>
<attribute name="VALUE" x="-1.016" y="246.761" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="-1.016" y="244.856" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="-1.016" y="243.078" size="1.27" layer="97"/>
<attribute name="TYPE" x="-1.016" y="241.3" size="1.27" layer="97"/>
</instance>
<instance part="GND7" gate="1" x="68.58" y="246.38" smashed="yes" rot="R90">
<attribute name="VALUE" x="71.12" y="243.84" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U4" gate="G$1" x="170.18" y="269.24" smashed="yes">
<attribute name="NAME" x="167.64" y="307.34" size="1.27" layer="95"/>
<attribute name="VALUE" x="167.64" y="241.3" size="1.27" layer="96"/>
</instance>
<instance part="J100" gate="G$1" x="205.74" y="254" smashed="yes">
<attribute name="NAME" x="205.74" y="299.72" size="1.27" layer="95"/>
<attribute name="VALUE" x="205.74" y="215.9" size="1.27" layer="95"/>
</instance>
<instance part="C23" gate="G$1" x="142.24" y="299.72" smashed="yes">
<attribute name="NAME" x="143.764" y="302.641" size="1.778" layer="95"/>
<attribute name="VALUE" x="143.764" y="297.561" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="143.764" y="295.656" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="143.764" y="293.878" size="1.27" layer="97"/>
<attribute name="TYPE" x="143.764" y="292.1" size="1.27" layer="97"/>
</instance>
<instance part="GND8" gate="1" x="127" y="297.18" smashed="yes" rot="R270">
<attribute name="VALUE" x="124.46" y="299.72" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="P+3" gate="1" x="127" y="304.8" smashed="yes" rot="R90">
<attribute name="VALUE" x="132.08" y="302.26" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R7" gate="G$1" x="190.5" y="289.56" smashed="yes" rot="R90">
<attribute name="NAME" x="189.0014" y="285.75" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="193.802" y="285.75" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="197.358" y="285.75" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="195.58" y="285.75" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="R5" gate="G$1" x="190.5" y="261.62" smashed="yes" rot="R90">
<attribute name="NAME" x="189.0014" y="257.81" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="193.802" y="257.81" size="1.778" layer="96" rot="R90"/>
<attribute name="PRECISION" x="197.358" y="257.81" size="1.27" layer="97" rot="R90"/>
<attribute name="PACKAGE" x="195.58" y="257.81" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="+3V35" gate="G$1" x="127" y="238.76" smashed="yes" rot="R90">
<attribute name="VALUE" x="132.08" y="236.22" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="177.8" y="233.68" smashed="yes">
<attribute name="NAME" x="173.99" y="235.1786" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.99" y="230.378" size="1.778" layer="96"/>
<attribute name="PRECISION" x="173.99" y="226.822" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="173.99" y="228.6" size="1.27" layer="97"/>
</instance>
<instance part="R1" gate="G$1" x="177.8" y="223.52" smashed="yes">
<attribute name="NAME" x="173.99" y="225.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="173.99" y="220.218" size="1.778" layer="96"/>
<attribute name="PRECISION" x="173.99" y="216.662" size="1.27" layer="97"/>
<attribute name="PACKAGE" x="173.99" y="218.44" size="1.27" layer="97"/>
</instance>
<instance part="U2" gate="G$1" x="10.16" y="226.06" smashed="yes">
<attribute name="NAME" x="7.62" y="233.68" size="1.27" layer="95"/>
<attribute name="VALUE" x="12.7" y="218.44" size="1.27" layer="96"/>
</instance>
<instance part="+3V34" gate="G$1" x="68.58" y="228.6" smashed="yes" rot="R270">
<attribute name="VALUE" x="63.5" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="10.16" y="208.28" smashed="yes">
<attribute name="VALUE" x="7.62" y="205.74" size="1.778" layer="96"/>
</instance>
<instance part="C11" gate="G$1" x="25.4" y="223.52" smashed="yes">
<attribute name="NAME" x="26.924" y="226.441" size="1.778" layer="95"/>
<attribute name="VALUE" x="26.924" y="221.361" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="26.924" y="219.456" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="26.924" y="217.678" size="1.27" layer="97"/>
<attribute name="TYPE" x="26.924" y="215.9" size="1.27" layer="97"/>
</instance>
<instance part="J2" gate="G$1" x="10.16" y="7.62" smashed="yes"/>
<instance part="J1" gate="G$1" x="20.32" y="7.62" smashed="yes"/>
<instance part="+3V36" gate="G$1" x="121.92" y="165.1" smashed="yes" rot="R90">
<attribute name="VALUE" x="127" y="162.56" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+4" gate="1" x="121.92" y="157.48" smashed="yes" rot="R90">
<attribute name="VALUE" x="127" y="154.94" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="D1" gate="G$1" x="-35.56" y="246.38" smashed="yes" rot="R90">
<attribute name="NAME" x="-36.0426" y="248.92" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-33.2486" y="248.92" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND11" gate="1" x="-35.56" y="233.68" smashed="yes">
<attribute name="VALUE" x="-38.1" y="231.14" size="1.778" layer="96"/>
</instance>
<instance part="U1" gate="G$1" x="-58.42" y="251.46" smashed="yes">
<attribute name="NAME" x="-60.96" y="264.16" size="1.27" layer="95"/>
<attribute name="VALUE" x="-60.96" y="238.76" size="1.27" layer="96"/>
</instance>
<instance part="P+6" gate="VCC" x="-83.82" y="261.62" smashed="yes" rot="R90">
<attribute name="VALUE" x="-81.28" y="259.08" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND9" gate="1" x="-83.82" y="243.84" smashed="yes" rot="R270">
<attribute name="VALUE" x="-86.36" y="246.38" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="GND10" gate="1" x="121.92" y="180.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="119.38" y="182.88" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="U$100" gate="G$1" x="160.02" y="149.86" smashed="yes"/>
<instance part="D2" gate="G$1" x="-25.4" y="297.18" smashed="yes" rot="R90">
<attribute name="NAME" x="-25.8826" y="299.72" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-23.0886" y="299.72" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="P+7" gate="VCC" x="121.92" y="172.72" smashed="yes" rot="R90">
<attribute name="VALUE" x="124.46" y="170.18" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="MP100" gate="G$1" x="38.1" y="10.16" smashed="yes"/>
<instance part="X100" gate="G$1" x="180.34" y="322.58" smashed="yes" rot="R90">
<attribute name="NAME" x="182.245" y="318.135" size="1.27" layer="95" font="vector" rot="R180"/>
<attribute name="VALUE" x="182.245" y="330.835" size="1.27" layer="96" font="vector" rot="R180"/>
</instance>
<instance part="GND12" gate="1" x="160.02" y="317.5" smashed="yes">
<attribute name="VALUE" x="157.48" y="314.96" size="1.778" layer="96"/>
</instance>
<instance part="P+5" gate="1" x="121.92" y="322.58" smashed="yes" rot="R90">
<attribute name="VALUE" x="127" y="320.04" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="P+2" gate="1" x="68.58" y="254" smashed="yes" rot="R270">
<attribute name="VALUE" x="63.5" y="256.54" size="1.778" layer="96"/>
</instance>
<instance part="J102" gate="G$1" x="142.24" y="322.58" smashed="yes">
<attribute name="NAME" x="139.7" y="325.12" size="1.778" layer="95"/>
<attribute name="VALUE" x="139.7" y="318.77" size="1.778" layer="96"/>
</instance>
<instance part="C22" gate="G$1" x="134.62" y="160.02" smashed="yes" rot="R90">
<attribute name="NAME" x="131.699" y="161.544" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="136.779" y="161.544" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="138.684" y="161.544" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="140.462" y="161.544" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="142.24" y="161.544" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="C21" gate="G$1" x="134.62" y="152.4" smashed="yes" rot="R90">
<attribute name="NAME" x="131.699" y="153.924" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="136.779" y="153.924" size="1.778" layer="96" rot="R90"/>
<attribute name="PACKAGE" x="138.684" y="153.924" size="1.27" layer="97" rot="R90"/>
<attribute name="VOLTAGE" x="140.462" y="153.924" size="1.27" layer="97" rot="R90"/>
<attribute name="TYPE" x="142.24" y="153.924" size="1.27" layer="97" rot="R90"/>
</instance>
<instance part="GND13" gate="1" x="111.76" y="154.94" smashed="yes" rot="R270">
<attribute name="VALUE" x="109.22" y="157.48" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="C24" gate="G$1" x="137.16" y="233.68" smashed="yes">
<attribute name="NAME" x="138.684" y="236.601" size="1.778" layer="95"/>
<attribute name="VALUE" x="138.684" y="231.521" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="138.684" y="229.616" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="138.684" y="227.838" size="1.27" layer="97"/>
<attribute name="TYPE" x="138.684" y="226.06" size="1.27" layer="97"/>
</instance>
<instance part="GND14" gate="1" x="137.16" y="226.06" smashed="yes">
<attribute name="VALUE" x="134.62" y="223.52" size="1.778" layer="96"/>
</instance>
<instance part="C5" gate="G$1" x="2.54" y="297.18" smashed="yes">
<attribute name="NAME" x="4.064" y="300.101" size="1.778" layer="95"/>
<attribute name="VALUE" x="4.064" y="295.021" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="4.064" y="293.116" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="4.064" y="291.338" size="1.27" layer="97"/>
<attribute name="TYPE" x="4.064" y="289.56" size="1.27" layer="97"/>
</instance>
<instance part="C10" gate="G$1" x="25.4" y="248.92" smashed="yes">
<attribute name="NAME" x="26.924" y="251.841" size="1.778" layer="95"/>
<attribute name="VALUE" x="26.924" y="246.761" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="26.924" y="244.856" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="26.924" y="243.078" size="1.27" layer="97"/>
<attribute name="TYPE" x="26.924" y="241.3" size="1.27" layer="97"/>
</instance>
<instance part="C8" gate="G$1" x="15.24" y="248.92" smashed="yes">
<attribute name="NAME" x="16.764" y="251.841" size="1.778" layer="95"/>
<attribute name="VALUE" x="16.764" y="246.761" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="16.764" y="244.856" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="16.764" y="243.078" size="1.27" layer="97"/>
<attribute name="TYPE" x="16.764" y="241.3" size="1.27" layer="97"/>
</instance>
<instance part="C4" gate="G$1" x="35.56" y="248.92" smashed="yes">
<attribute name="NAME" x="37.084" y="251.841" size="1.778" layer="95"/>
<attribute name="VALUE" x="37.084" y="246.761" size="1.778" layer="96"/>
<attribute name="VOLTAGE" x="37.084" y="243.078" size="1.27" layer="97"/>
<attribute name="TYPE" x="37.084" y="241.3" size="1.27" layer="97"/>
</instance>
<instance part="C2" gate="G$1" x="45.72" y="248.92" smashed="yes">
<attribute name="NAME" x="47.244" y="251.841" size="1.778" layer="95"/>
<attribute name="VALUE" x="47.244" y="246.761" size="1.778" layer="96"/>
<attribute name="VOLTAGE" x="47.244" y="243.078" size="1.27" layer="97"/>
<attribute name="TYPE" x="47.244" y="241.3" size="1.27" layer="97"/>
</instance>
<instance part="C1" gate="G$1" x="55.88" y="248.92" smashed="yes">
<attribute name="NAME" x="57.404" y="251.841" size="1.778" layer="95"/>
<attribute name="VALUE" x="57.404" y="246.761" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="57.404" y="244.856" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="57.404" y="243.078" size="1.27" layer="97"/>
<attribute name="TYPE" x="57.404" y="241.3" size="1.27" layer="97"/>
</instance>
<instance part="C15" gate="G$1" x="35.56" y="223.52" smashed="yes">
<attribute name="NAME" x="37.084" y="226.441" size="1.778" layer="95"/>
<attribute name="VALUE" x="37.084" y="221.361" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="37.084" y="219.456" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="37.084" y="217.678" size="1.27" layer="97"/>
<attribute name="TYPE" x="37.084" y="215.9" size="1.27" layer="97"/>
</instance>
<instance part="C13" gate="G$1" x="45.72" y="223.52" smashed="yes">
<attribute name="NAME" x="47.244" y="226.441" size="1.778" layer="95"/>
<attribute name="VALUE" x="47.244" y="221.361" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="47.244" y="219.456" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="47.244" y="217.678" size="1.27" layer="97"/>
<attribute name="TYPE" x="47.244" y="215.9" size="1.27" layer="97"/>
</instance>
<instance part="C12" gate="G$1" x="55.88" y="223.52" smashed="yes">
<attribute name="NAME" x="57.404" y="226.441" size="1.778" layer="95"/>
<attribute name="VALUE" x="57.404" y="221.361" size="1.778" layer="96"/>
<attribute name="PACKAGE" x="57.404" y="219.456" size="1.27" layer="97"/>
<attribute name="VOLTAGE" x="57.404" y="217.678" size="1.27" layer="97"/>
<attribute name="TYPE" x="57.404" y="215.9" size="1.27" layer="97"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="43.18" y1="302.26" x2="33.02" y2="302.26" width="0.1524" layer="91"/>
<label x="33.02" y="302.26" size="1.778" layer="95"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<pinref part="Y1" gate="G$1" pin="2"/>
<wire x1="17.78" y1="106.68" x2="22.86" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="GND"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="38.1" y1="144.78" x2="25.4" y2="144.78" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="25.4" y1="144.78" x2="15.24" y2="144.78" width="0.1524" layer="91"/>
<junction x="25.4" y="144.78"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="15.24" y1="144.78" x2="5.08" y2="144.78" width="0.1524" layer="91"/>
<junction x="15.24" y="144.78"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="5.08" y1="144.78" x2="-5.08" y2="144.78" width="0.1524" layer="91"/>
<junction x="5.08" y="144.78"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="144.78" x2="-15.24" y2="144.78" width="0.1524" layer="91"/>
<junction x="-5.08" y="144.78"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="-15.24" y1="144.78" x2="-25.4" y2="144.78" width="0.1524" layer="91"/>
<junction x="-15.24" y="144.78"/>
<wire x1="-25.4" y1="144.78" x2="-43.18" y2="144.78" width="0.1524" layer="91"/>
<junction x="-25.4" y="144.78"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<pinref part="J101" gate="G$1" pin="GND"/>
<wire x1="-25.4" y1="172.72" x2="-12.7" y2="172.72" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="P1"/>
<wire x1="-12.7" y1="172.72" x2="-10.16" y2="172.72" width="0.1524" layer="91"/>
<wire x1="0" y1="180.34" x2="-12.7" y2="180.34" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="180.34" x2="-12.7" y2="172.72" width="0.1524" layer="91"/>
<junction x="-12.7" y="172.72"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="-7.62" y1="292.1" x2="-17.78" y2="292.1" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="292.1" x2="-25.4" y2="292.1" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="292.1" x2="-33.02" y2="292.1" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="294.64" x2="-7.62" y2="292.1" width="0.1524" layer="91"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="-17.78" y1="294.64" x2="-17.78" y2="292.1" width="0.1524" layer="91"/>
<junction x="-17.78" y="292.1"/>
<junction x="-25.4" y="292.1"/>
<pinref part="D2" gate="G$1" pin="A"/>
<wire x1="-25.4" y1="294.64" x2="-25.4" y2="292.1" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="2.54" y1="294.64" x2="2.54" y2="292.1" width="0.1524" layer="91"/>
<wire x1="2.54" y1="292.1" x2="-7.62" y2="292.1" width="0.1524" layer="91"/>
<junction x="-7.62" y="292.1"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="GND"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="152.4" y1="297.18" x2="142.24" y2="297.18" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="142.24" y1="297.18" x2="129.54" y2="297.18" width="0.1524" layer="91"/>
<junction x="142.24" y="297.18"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="10.16" y1="215.9" x2="10.16" y2="213.36" width="0.1524" layer="91"/>
<wire x1="10.16" y1="213.36" x2="10.16" y2="210.82" width="0.1524" layer="91"/>
<wire x1="10.16" y1="213.36" x2="25.4" y2="213.36" width="0.1524" layer="91"/>
<junction x="10.16" y="213.36"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="25.4" y1="213.36" x2="25.4" y2="220.98" width="0.1524" layer="91"/>
<junction x="25.4" y="213.36"/>
<wire x1="45.72" y1="213.36" x2="35.56" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="35.56" y1="213.36" x2="25.4" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="35.56" y1="220.98" x2="35.56" y2="213.36" width="0.1524" layer="91"/>
<junction x="35.56" y="213.36"/>
<wire x1="45.72" y1="220.98" x2="45.72" y2="213.36" width="0.1524" layer="91"/>
<wire x1="55.88" y1="220.98" x2="55.88" y2="213.36" width="0.1524" layer="91"/>
<wire x1="55.88" y1="213.36" x2="45.72" y2="213.36" width="0.1524" layer="91"/>
<junction x="45.72" y="213.36"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="-35.56" y1="243.84" x2="-35.56" y2="236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PGND"/>
<wire x1="-76.2" y1="243.84" x2="-73.66" y2="243.84" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="-73.66" y1="246.38" x2="-76.2" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="246.38" x2="-76.2" y2="243.84" width="0.1524" layer="91"/>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="-81.28" y1="243.84" x2="-76.2" y2="243.84" width="0.1524" layer="91"/>
<junction x="-76.2" y="243.84"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="U$100" gate="G$1" pin="GND"/>
<wire x1="124.46" y1="180.34" x2="149.86" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X100" gate="G$1" pin="GND"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="175.26" y1="320.04" x2="160.02" y2="320.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="114.3" y1="154.94" x2="116.84" y2="154.94" width="0.1524" layer="91"/>
<wire x1="116.84" y1="154.94" x2="116.84" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="116.84" y1="160.02" x2="129.54" y2="160.02" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="129.54" y1="152.4" x2="116.84" y2="152.4" width="0.1524" layer="91"/>
<wire x1="116.84" y1="152.4" x2="116.84" y2="154.94" width="0.1524" layer="91"/>
<junction x="116.84" y="154.94"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="137.16" y1="228.6" x2="137.16" y2="231.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GND"/>
<pinref part="C1" gate="G$1" pin="2"/>
<wire x1="66.04" y1="246.38" x2="55.88" y2="246.38" width="0.1524" layer="91"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="55.88" y1="246.38" x2="45.72" y2="246.38" width="0.1524" layer="91"/>
<junction x="55.88" y="246.38"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="45.72" y1="246.38" x2="35.56" y2="246.38" width="0.1524" layer="91"/>
<junction x="45.72" y="246.38"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="35.56" y1="246.38" x2="25.4" y2="246.38" width="0.1524" layer="91"/>
<junction x="35.56" y="246.38"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="25.4" y1="246.38" x2="15.24" y2="246.38" width="0.1524" layer="91"/>
<junction x="25.4" y="246.38"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="15.24" y1="246.38" x2="-2.54" y2="246.38" width="0.1524" layer="91"/>
<junction x="15.24" y="246.38"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="D5" gate="G$1" pin="A"/>
<wire x1="66.04" y1="302.26" x2="76.2" y2="302.26" width="0.1524" layer="91"/>
<wire x1="76.2" y1="302.26" x2="76.2" y2="292.1" width="0.1524" layer="91"/>
<wire x1="76.2" y1="292.1" x2="76.2" y2="287.02" width="0.1524" layer="91"/>
<wire x1="76.2" y1="287.02" x2="76.2" y2="281.94" width="0.1524" layer="91"/>
<pinref part="D3" gate="G$1" pin="A"/>
<wire x1="66.04" y1="281.94" x2="76.2" y2="281.94" width="0.1524" layer="91"/>
<pinref part="D4" gate="G$1" pin="A"/>
<wire x1="66.04" y1="292.1" x2="76.2" y2="292.1" width="0.1524" layer="91"/>
<junction x="76.2" y="292.1"/>
<wire x1="76.2" y1="287.02" x2="81.28" y2="287.02" width="0.1524" layer="91"/>
<junction x="76.2" y="287.02"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="152.4" x2="-25.4" y2="152.4" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="152.4" x2="-15.24" y2="152.4" width="0.1524" layer="91"/>
<junction x="-25.4" y="152.4"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="-15.24" y1="152.4" x2="-5.08" y2="152.4" width="0.1524" layer="91"/>
<junction x="-15.24" y="152.4"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="152.4" x2="5.08" y2="152.4" width="0.1524" layer="91"/>
<junction x="-5.08" y="152.4"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="5.08" y1="152.4" x2="15.24" y2="152.4" width="0.1524" layer="91"/>
<junction x="5.08" y="152.4"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="25.4" y1="152.4" x2="25.4" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="AVCC"/>
<wire x1="25.4" y1="157.48" x2="38.1" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="VCC"/>
<wire x1="38.1" y1="152.4" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<junction x="25.4" y="152.4"/>
<wire x1="15.24" y1="152.4" x2="25.4" y2="152.4" width="0.1524" layer="91"/>
<junction x="15.24" y="152.4"/>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
<pinref part="J101" gate="G$1" pin="VCC"/>
<wire x1="-25.4" y1="167.64" x2="-10.16" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
<pinref part="J100" gate="G$1" pin="3V3-LED"/>
<wire x1="129.54" y1="238.76" x2="137.16" y2="238.76" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="1DE"/>
<wire x1="137.16" y1="238.76" x2="147.32" y2="238.76" width="0.1524" layer="91"/>
<wire x1="147.32" y1="238.76" x2="195.58" y2="238.76" width="0.1524" layer="91"/>
<wire x1="152.4" y1="279.4" x2="147.32" y2="279.4" width="0.1524" layer="91"/>
<wire x1="147.32" y1="279.4" x2="147.32" y2="251.46" width="0.1524" layer="91"/>
<junction x="147.32" y="238.76"/>
<pinref part="U4" gate="G$1" pin="2DE"/>
<wire x1="147.32" y1="251.46" x2="147.32" y2="238.76" width="0.1524" layer="91"/>
<wire x1="152.4" y1="251.46" x2="147.32" y2="251.46" width="0.1524" layer="91"/>
<junction x="147.32" y="251.46"/>
<pinref part="C24" gate="G$1" pin="1"/>
<junction x="137.16" y="238.76"/>
</segment>
<segment>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
<pinref part="U$100" gate="G$1" pin="+3V3"/>
<wire x1="124.46" y1="165.1" x2="137.16" y2="165.1" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="137.16" y1="165.1" x2="149.86" y2="165.1" width="0.1524" layer="91"/>
<wire x1="137.16" y1="160.02" x2="137.16" y2="165.1" width="0.1524" layer="91"/>
<junction x="137.16" y="165.1"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="VOUT"/>
<wire x1="22.86" y1="228.6" x2="25.4" y2="228.6" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="1"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="25.4" y1="228.6" x2="35.56" y2="228.6" width="0.1524" layer="91"/>
<junction x="25.4" y="228.6"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="35.56" y1="228.6" x2="45.72" y2="228.6" width="0.1524" layer="91"/>
<junction x="35.56" y="228.6"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="45.72" y1="228.6" x2="55.88" y2="228.6" width="0.1524" layer="91"/>
<junction x="45.72" y="228.6"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<wire x1="55.88" y1="228.6" x2="66.04" y2="228.6" width="0.1524" layer="91"/>
<junction x="55.88" y="228.6"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<pinref part="D5" gate="G$1" pin="C"/>
<wire x1="53.34" y1="302.26" x2="58.42" y2="302.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="D4" gate="G$1" pin="C"/>
<wire x1="53.34" y1="292.1" x2="58.42" y2="292.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="D3" gate="G$1" pin="C"/>
<wire x1="53.34" y1="281.94" x2="58.42" y2="281.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STLCLK" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="43.18" y1="292.1" x2="33.02" y2="292.1" width="0.1524" layer="91"/>
<label x="33.02" y="292.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA3"/>
<wire x1="88.9" y1="165.1" x2="111.76" y2="165.1" width="0.1524" layer="91"/>
<label x="91.44" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="STLERR" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="43.18" y1="281.94" x2="33.02" y2="281.94" width="0.1524" layer="91"/>
<label x="33.02" y="281.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PA4"/>
<wire x1="88.9" y1="162.56" x2="111.76" y2="162.56" width="0.1524" layer="91"/>
<label x="91.44" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="1"/>
<pinref part="U3" gate="G$1" pin="PR0/XTAL2"/>
<wire x1="30.48" y1="114.3" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="Y1" gate="G$1" pin="3"/>
<pinref part="U3" gate="G$1" pin="PR1/XTAL1"/>
<wire x1="30.48" y1="99.06" x2="38.1" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="J101" gate="G$1" pin="PDI_CLK"/>
<pinref part="U3" gate="G$1" pin="RESET/PDI_CLK"/>
<wire x1="20.32" y1="172.72" x2="22.86" y2="172.72" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="S1"/>
<wire x1="22.86" y1="172.72" x2="38.1" y2="172.72" width="0.1524" layer="91"/>
<wire x1="10.16" y1="180.34" x2="22.86" y2="180.34" width="0.1524" layer="91"/>
<wire x1="22.86" y1="180.34" x2="22.86" y2="177.8" width="0.1524" layer="91"/>
<wire x1="22.86" y1="177.8" x2="22.86" y2="172.72" width="0.1524" layer="91"/>
<junction x="22.86" y="172.72"/>
<label x="25.4" y="172.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="PDI/DATA" class="0">
<segment>
<pinref part="J101" gate="G$1" pin="PDI_DATA"/>
<pinref part="U3" gate="G$1" pin="PDI_DATA"/>
<wire x1="20.32" y1="167.64" x2="38.1" y2="167.64" width="0.1524" layer="91"/>
<label x="25.4" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="BST" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="-43.18" y1="261.62" x2="-35.56" y2="261.62" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="BST"/>
<label x="-43.18" y="261.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="VSW" class="0">
<segment>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="-43.18" y1="254" x2="-35.56" y2="254" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="-35.56" y1="254" x2="-25.4" y2="254" width="0.1524" layer="91"/>
<junction x="-35.56" y="254"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="-35.56" y1="248.92" x2="-35.56" y2="254" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VSW"/>
<label x="-43.18" y="254" size="1.778" layer="95"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="P+1" gate="VCC" pin="VCC"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="-33.02" y1="302.26" x2="-25.4" y2="302.26" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="-25.4" y1="302.26" x2="-17.78" y2="302.26" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="302.26" x2="-7.62" y2="302.26" width="0.1524" layer="91"/>
<junction x="-17.78" y="302.26"/>
<junction x="-25.4" y="302.26"/>
<pinref part="D2" gate="G$1" pin="C"/>
<wire x1="-25.4" y1="299.72" x2="-25.4" y2="302.26" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="302.26" x2="2.54" y2="302.26" width="0.1524" layer="91"/>
<junction x="-7.62" y="302.26"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VCC"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
<wire x1="-81.28" y1="261.62" x2="-73.66" y2="261.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+7" gate="VCC" pin="VCC"/>
<pinref part="U$100" gate="G$1" pin="VCC"/>
<wire x1="124.46" y1="172.72" x2="149.86" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+3" gate="1" pin="+5V"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="129.54" y1="304.8" x2="142.24" y2="304.8" width="0.1524" layer="91"/>
<pinref part="U4" gate="G$1" pin="VCC"/>
<wire x1="142.24" y1="304.8" x2="152.4" y2="304.8" width="0.1524" layer="91"/>
<junction x="142.24" y="304.8"/>
</segment>
<segment>
<pinref part="P+4" gate="1" pin="+5V"/>
<pinref part="U$100" gate="G$1" pin="+5V"/>
<wire x1="124.46" y1="157.48" x2="137.16" y2="157.48" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="137.16" y1="157.48" x2="149.86" y2="157.48" width="0.1524" layer="91"/>
<wire x1="137.16" y1="152.4" x2="137.16" y2="157.48" width="0.1524" layer="91"/>
<junction x="137.16" y="157.48"/>
</segment>
<segment>
<pinref part="P+5" gate="1" pin="+5V"/>
<pinref part="J102" gate="G$1" pin="1"/>
<wire x1="124.46" y1="322.58" x2="137.16" y2="322.58" width="0.1524" layer="91"/>
<wire x1="137.16" y1="322.58" x2="137.16" y2="317.5" width="0.1524" layer="91"/>
<junction x="137.16" y="322.58"/>
<wire x1="137.16" y1="317.5" x2="147.32" y2="317.5" width="0.1524" layer="91"/>
<pinref part="J102" gate="G$1" pin="2"/>
<pinref part="X100" gate="G$1" pin="VBUS"/>
<wire x1="147.32" y1="322.58" x2="175.26" y2="322.58" width="0.1524" layer="91"/>
<wire x1="147.32" y1="317.5" x2="147.32" y2="322.58" width="0.1524" layer="91"/>
<junction x="147.32" y="322.58"/>
</segment>
<segment>
<wire x1="-43.18" y1="246.38" x2="-7.62" y2="246.38" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="246.38" x2="-7.62" y2="254" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="254" x2="-10.16" y2="254" width="0.1524" layer="91"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="254" x2="-5.08" y2="254" width="0.1524" layer="91"/>
<junction x="-7.62" y="254"/>
<wire x1="-5.08" y1="254" x2="-2.54" y2="254" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="VIN"/>
<wire x1="-2.54" y1="228.6" x2="-5.08" y2="228.6" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="228.6" x2="-5.08" y2="254" width="0.1524" layer="91"/>
<junction x="-5.08" y="254"/>
<pinref part="U2" gate="G$1" pin="EN"/>
<wire x1="-2.54" y1="223.52" x2="-5.08" y2="223.52" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="223.52" x2="-5.08" y2="228.6" width="0.1524" layer="91"/>
<junction x="-5.08" y="228.6"/>
<pinref part="U1" gate="G$1" pin="FB"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="254" x2="15.24" y2="254" width="0.1524" layer="91"/>
<junction x="-2.54" y="254"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="15.24" y1="254" x2="25.4" y2="254" width="0.1524" layer="91"/>
<junction x="15.24" y="254"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="25.4" y1="254" x2="35.56" y2="254" width="0.1524" layer="91"/>
<junction x="25.4" y="254"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="35.56" y1="254" x2="45.72" y2="254" width="0.1524" layer="91"/>
<junction x="35.56" y="254"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="45.72" y1="254" x2="55.88" y2="254" width="0.1524" layer="91"/>
<junction x="45.72" y="254"/>
<pinref part="P+2" gate="1" pin="+5V"/>
<wire x1="55.88" y1="254" x2="66.04" y2="254" width="0.1524" layer="91"/>
<junction x="55.88" y="254"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1A"/>
<pinref part="J100" gate="G$1" pin="CLKRX-A"/>
<wire x1="187.96" y1="294.64" x2="190.5" y2="294.64" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="190.5" y1="294.64" x2="195.58" y2="294.64" width="0.1524" layer="91"/>
<junction x="190.5" y="294.64"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1B"/>
<pinref part="J100" gate="G$1" pin="CLKRX-B"/>
<wire x1="187.96" y1="284.48" x2="190.5" y2="284.48" width="0.1524" layer="91"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="190.5" y1="284.48" x2="195.58" y2="284.48" width="0.1524" layer="91"/>
<junction x="190.5" y="284.48"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1Y"/>
<pinref part="J100" gate="G$1" pin="TX-Y"/>
<wire x1="187.96" y1="279.4" x2="195.58" y2="279.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1Z"/>
<pinref part="J100" gate="G$1" pin="TX-Z"/>
<wire x1="187.96" y1="274.32" x2="195.58" y2="274.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2B"/>
<pinref part="J100" gate="G$1" pin="RX-B"/>
<wire x1="187.96" y1="266.7" x2="190.5" y2="266.7" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="190.5" y1="266.7" x2="195.58" y2="266.7" width="0.1524" layer="91"/>
<junction x="190.5" y="266.7"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2A"/>
<pinref part="J100" gate="G$1" pin="RX-A"/>
<wire x1="187.96" y1="256.54" x2="190.5" y2="256.54" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="190.5" y1="256.54" x2="195.58" y2="256.54" width="0.1524" layer="91"/>
<junction x="190.5" y="256.54"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2Z"/>
<pinref part="J100" gate="G$1" pin="CLKTX-Z"/>
<wire x1="187.96" y1="251.46" x2="195.58" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="2Y"/>
<pinref part="J100" gate="G$1" pin="CLKRX-Y"/>
<wire x1="187.96" y1="246.38" x2="195.58" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ATK0-RXLED" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="172.72" y1="233.68" x2="147.32" y2="233.68" width="0.1524" layer="91"/>
<label x="147.32" y="233.68" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PF1/XCK0"/>
<wire x1="88.9" y1="55.88" x2="106.68" y2="55.88" width="0.1524" layer="91"/>
<label x="91.44" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK0-TXLED" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="172.72" y1="223.52" x2="147.32" y2="223.52" width="0.1524" layer="91"/>
<label x="147.32" y="223.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PF0"/>
<wire x1="88.9" y1="58.42" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<label x="91.44" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK0-RX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PF2/RXD0"/>
<wire x1="88.9" y1="53.34" x2="106.68" y2="53.34" width="0.1524" layer="91"/>
<label x="91.44" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="2R"/>
<wire x1="152.4" y1="261.62" x2="129.54" y2="261.62" width="0.1524" layer="91"/>
<label x="129.54" y="261.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK0-TX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PF3/TXD0"/>
<wire x1="88.9" y1="50.8" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<label x="91.44" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="1D"/>
<wire x1="152.4" y1="274.32" x2="129.54" y2="274.32" width="0.1524" layer="91"/>
<label x="129.54" y="274.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK-CLKOUT" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PF4"/>
<wire x1="88.9" y1="48.26" x2="106.68" y2="48.26" width="0.1524" layer="91"/>
<label x="91.44" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U4" gate="G$1" pin="2D"/>
<wire x1="152.4" y1="246.38" x2="129.54" y2="246.38" width="0.1524" layer="91"/>
<label x="129.54" y="246.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="ATK0-CLKIN" class="0">
<segment>
<pinref part="U4" gate="G$1" pin="1R"/>
<wire x1="152.4" y1="289.56" x2="129.54" y2="289.56" width="0.1524" layer="91"/>
<label x="129.54" y="289.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U3" gate="G$1" pin="PF7"/>
<wire x1="88.9" y1="40.64" x2="106.68" y2="40.64" width="0.1524" layer="91"/>
<label x="91.44" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="RXLED" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="2"/>
<pinref part="J100" gate="G$1" pin="LED-GRN-CATHODE"/>
<wire x1="182.88" y1="233.68" x2="195.58" y2="233.68" width="0.1524" layer="91"/>
<label x="182.88" y="233.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXLED" class="0">
<segment>
<pinref part="J100" gate="G$1" pin="LED-YLW-CATHODE"/>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="195.58" y1="223.52" x2="182.88" y2="223.52" width="0.1524" layer="91"/>
<label x="182.88" y="223.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWMALS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC0/SDA"/>
<wire x1="88.9" y1="127" x2="106.68" y2="127" width="0.1524" layer="91"/>
<label x="91.44" y="127" size="1.778" layer="95"/>
<pinref part="U$100" gate="G$1" pin="C0/PWMALS---PC0/PWMALS/CSN/SDA"/>
<wire x1="149.86" y1="127" x2="106.68" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWMAHS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC1/SCL/XCK0"/>
<wire x1="88.9" y1="124.46" x2="106.68" y2="124.46" width="0.1524" layer="91"/>
<label x="91.44" y="124.46" size="1.778" layer="95"/>
<pinref part="U$100" gate="G$1" pin="C1/PWMAHS---PC1/PWMAHS/SCK/SCL"/>
<wire x1="149.86" y1="124.46" x2="106.68" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWMBLS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC2/RXD0"/>
<wire x1="88.9" y1="121.92" x2="106.68" y2="121.92" width="0.1524" layer="91"/>
<label x="91.44" y="121.92" size="1.778" layer="95"/>
<pinref part="U$100" gate="G$1" pin="C2/PWMBLS---PC2/PWMBLS/MISO/UARTRX"/>
<wire x1="149.86" y1="121.92" x2="106.68" y2="121.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWMBHS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC3/TXD0"/>
<wire x1="88.9" y1="119.38" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<label x="91.44" y="119.38" size="1.778" layer="95"/>
<pinref part="U$100" gate="G$1" pin="C3/PWMBHS---PC3/PWMBHS/MOSI/UARTTX"/>
<wire x1="149.86" y1="119.38" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWMCLS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC4/SS"/>
<wire x1="88.9" y1="116.84" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<label x="91.44" y="116.84" size="1.778" layer="95"/>
<pinref part="U$100" gate="G$1" pin="C4/PWMCLS---PC4/PWMCLS/CSN"/>
<wire x1="149.86" y1="116.84" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWMCHS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC5/XCK1/MOSI"/>
<wire x1="88.9" y1="114.3" x2="106.68" y2="114.3" width="0.1524" layer="91"/>
<label x="91.44" y="114.3" size="1.778" layer="95"/>
<pinref part="U$100" gate="G$1" pin="C5/PWMCHS---PC5/PWMCHS/SCK"/>
<wire x1="149.86" y1="114.3" x2="106.68" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWMDLS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC6/RXD1/MISO"/>
<wire x1="88.9" y1="111.76" x2="106.68" y2="111.76" width="0.1524" layer="91"/>
<label x="91.44" y="111.76" size="1.778" layer="95"/>
<pinref part="U$100" gate="G$1" pin="C6/PWMDLS---PC6/PWMDLS/MISO/UARTRX"/>
<wire x1="149.86" y1="111.76" x2="106.68" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWMDHS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PC7/TXD1/SCK"/>
<wire x1="88.9" y1="109.22" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
<label x="91.44" y="109.22" size="1.778" layer="95"/>
<pinref part="U$100" gate="G$1" pin="C7/PWMDHS---PC7/PWMDHS/MOSI/UARTTX"/>
<wire x1="149.86" y1="109.22" x2="106.68" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="USBDM" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PD6/RXD1/MISO/D-"/>
<wire x1="88.9" y1="88.9" x2="106.68" y2="88.9" width="0.1524" layer="91"/>
<label x="91.44" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X100" gate="G$1" pin="D-"/>
<wire x1="175.26" y1="325.12" x2="160.02" y2="325.12" width="0.1524" layer="91"/>
<label x="160.02" y="325.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="USBDP" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PD7/TXD1/SCK/D+"/>
<wire x1="88.9" y1="86.36" x2="106.68" y2="86.36" width="0.1524" layer="91"/>
<label x="91.44" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X100" gate="G$1" pin="D+"/>
<wire x1="175.26" y1="327.66" x2="160.02" y2="327.66" width="0.1524" layer="91"/>
<label x="160.02" y="327.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART1/CSN" class="0">
<segment>
<pinref part="U$100" gate="G$1" pin="D0/USART/CSN---PD0/PWMA/CSN"/>
<pinref part="U3" gate="G$1" pin="PD0"/>
<wire x1="149.86" y1="104.14" x2="88.9" y2="104.14" width="0.1524" layer="91"/>
<label x="91.44" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART1/SCK" class="0">
<segment>
<pinref part="U$100" gate="G$1" pin="D1/USART/CLK---PD1/PWMB/SCK"/>
<pinref part="U3" gate="G$1" pin="PD1/XCK0"/>
<wire x1="149.86" y1="101.6" x2="88.9" y2="101.6" width="0.1524" layer="91"/>
<label x="91.44" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART1/RX" class="0">
<segment>
<pinref part="U$100" gate="G$1" pin="D2/USART/RX---PD2/PWMC/MISO/UARTRX"/>
<pinref part="U3" gate="G$1" pin="PD2/RXD0"/>
<wire x1="149.86" y1="99.06" x2="88.9" y2="99.06" width="0.1524" layer="91"/>
<label x="91.44" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART1/TX" class="0">
<segment>
<pinref part="U$100" gate="G$1" pin="D3/USART/TX---PD3/PWMD/MOSI/UARTTX"/>
<pinref part="U3" gate="G$1" pin="PD3/TXD0"/>
<wire x1="149.86" y1="96.52" x2="88.9" y2="96.52" width="0.1524" layer="91"/>
<label x="91.44" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="TW1/SCL" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PE1/SCL/XCK0"/>
<wire x1="88.9" y1="78.74" x2="114.3" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="E1/SCL---PE1"/>
<wire x1="149.86" y1="81.28" x2="132.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="132.08" y1="81.28" x2="132.08" y2="78.74" width="0.1524" layer="91"/>
<wire x1="132.08" y1="78.74" x2="114.3" y2="78.74" width="0.1524" layer="91"/>
<label x="91.44" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART2/CSN" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PE4/SS"/>
<wire x1="88.9" y1="71.12" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="E4/USART/CSN---PE4/PWMA/CSN"/>
<wire x1="149.86" y1="68.58" x2="137.16" y2="68.58" width="0.1524" layer="91"/>
<wire x1="137.16" y1="68.58" x2="137.16" y2="71.12" width="0.1524" layer="91"/>
<wire x1="137.16" y1="71.12" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<label x="91.44" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART2/RX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PE6/RXD1/MISO"/>
<wire x1="88.9" y1="66.04" x2="127" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="E6/USART/RX---PE6/UARTRX/MISO"/>
<wire x1="149.86" y1="63.5" x2="132.08" y2="63.5" width="0.1524" layer="91"/>
<wire x1="132.08" y1="63.5" x2="132.08" y2="66.04" width="0.1524" layer="91"/>
<wire x1="132.08" y1="66.04" x2="127" y2="66.04" width="0.1524" layer="91"/>
<label x="91.44" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART2/TX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PE7/TXD1/SCK"/>
<wire x1="129.54" y1="63.5" x2="88.9" y2="63.5" width="0.1524" layer="91"/>
<wire x1="129.54" y1="63.5" x2="129.54" y2="60.96" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="E7/USART/TX---PE7/UARTTX/MOSI"/>
<wire x1="129.54" y1="60.96" x2="149.86" y2="60.96" width="0.1524" layer="91"/>
<label x="91.44" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART1/RX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PE2/RXD0"/>
<wire x1="116.84" y1="76.2" x2="88.9" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="E2/USARTRX---PE2"/>
<wire x1="116.84" y1="76.2" x2="149.86" y2="76.2" width="0.1524" layer="91"/>
<label x="91.44" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="UART1/TX" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PE3/TXD0"/>
<wire x1="88.9" y1="73.66" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="E3/USARTTX---PE3"/>
<wire x1="149.86" y1="73.66" x2="119.38" y2="73.66" width="0.1524" layer="91"/>
<label x="91.44" y="73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="TW1/SDA" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PE0/SDA"/>
<wire x1="88.9" y1="81.28" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<wire x1="111.76" y1="81.28" x2="129.54" y2="81.28" width="0.1524" layer="91"/>
<wire x1="129.54" y1="81.28" x2="129.54" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="E0/SDA---PE0"/>
<wire x1="129.54" y1="83.82" x2="149.86" y2="83.82" width="0.1524" layer="91"/>
<label x="91.44" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="USART2/SCK" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PE5/XCK1/MOSI"/>
<wire x1="124.46" y1="68.58" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<wire x1="124.46" y1="68.58" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
<wire x1="134.62" y1="68.58" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="E5/USART/SCK---PE5/PWMB/SCK"/>
<wire x1="134.62" y1="66.04" x2="149.86" y2="66.04" width="0.1524" layer="91"/>
<label x="91.44" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWMELS" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PD4/SS"/>
<wire x1="88.9" y1="93.98" x2="132.08" y2="93.98" width="0.1524" layer="91"/>
<wire x1="132.08" y1="93.98" x2="132.08" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U$100" gate="G$1" pin="D4/PWMELS---PD4"/>
<wire x1="132.08" y1="91.44" x2="149.86" y2="91.44" width="0.1524" layer="91"/>
<label x="91.44" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWMEHS" class="0">
<segment>
<pinref part="U$100" gate="G$1" pin="D5/PWMEHS---PD5"/>
<wire x1="149.86" y1="88.9" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="129.54" y1="88.9" x2="129.54" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U3" gate="G$1" pin="PD5/XCK/MOSI"/>
<wire x1="129.54" y1="91.44" x2="88.9" y2="91.44" width="0.1524" layer="91"/>
<label x="91.44" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC0/AREF" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB0/AREF"/>
<wire x1="88.9" y1="149.86" x2="111.76" y2="149.86" width="0.1524" layer="91"/>
<label x="91.44" y="149.86" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="A7/ADC---PB7/ADC/ADCOUT"/>
<wire x1="149.86" y1="132.08" x2="116.84" y2="132.08" width="0.1524" layer="91"/>
<label x="116.84" y="132.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB1"/>
<wire x1="88.9" y1="147.32" x2="111.76" y2="147.32" width="0.1524" layer="91"/>
<label x="91.44" y="147.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="A6/ADC---PB6/ADC/ADCOUT"/>
<wire x1="149.86" y1="134.62" x2="116.84" y2="134.62" width="0.1524" layer="91"/>
<label x="116.84" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC2/DAC0" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB2/DAC0"/>
<wire x1="88.9" y1="144.78" x2="111.76" y2="144.78" width="0.1524" layer="91"/>
<label x="91.44" y="144.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="A5/ADC---PB5/ADC"/>
<wire x1="149.86" y1="137.16" x2="116.84" y2="137.16" width="0.1524" layer="91"/>
<label x="116.84" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC3/DAC1" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB3/DAC1"/>
<wire x1="88.9" y1="142.24" x2="111.76" y2="142.24" width="0.1524" layer="91"/>
<label x="91.44" y="142.24" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="A4/ADC---PB4/ADC"/>
<wire x1="149.86" y1="139.7" x2="116.84" y2="139.7" width="0.1524" layer="91"/>
<label x="116.84" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC4" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB4"/>
<wire x1="88.9" y1="139.7" x2="111.76" y2="139.7" width="0.1524" layer="91"/>
<label x="91.44" y="139.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="A3/ADC/DAC---PB3/ADC/DAC"/>
<wire x1="149.86" y1="142.24" x2="116.84" y2="142.24" width="0.1524" layer="91"/>
<label x="116.84" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC5" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB5"/>
<wire x1="88.9" y1="137.16" x2="111.76" y2="137.16" width="0.1524" layer="91"/>
<label x="91.44" y="137.16" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="A2/ADC/DAC---PB2/ADC/DAC"/>
<wire x1="149.86" y1="144.78" x2="116.84" y2="144.78" width="0.1524" layer="91"/>
<label x="116.84" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC6" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB6"/>
<wire x1="88.9" y1="134.62" x2="111.76" y2="134.62" width="0.1524" layer="91"/>
<label x="91.44" y="134.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="A1/ADC---PB1/ADC"/>
<wire x1="149.86" y1="147.32" x2="116.84" y2="147.32" width="0.1524" layer="91"/>
<label x="116.84" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC7" class="0">
<segment>
<pinref part="U3" gate="G$1" pin="PB7"/>
<wire x1="88.9" y1="132.08" x2="111.76" y2="132.08" width="0.1524" layer="91"/>
<label x="91.44" y="132.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="A0/AREF---PB0/ADC"/>
<wire x1="149.86" y1="149.86" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<label x="116.84" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
